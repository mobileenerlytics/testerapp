echo "This script will run all the gtests and provide a 'report' of the passing and failing tests"
echo
echo "To use this make sure include $(BUILD_EXECUTABLE) is uncommented and "
echo "include $(BUILD_SHARED_LIBRARY) is commented for tracereducetest in Android.mk"
echo
echo "To debug and set breakpoints see GoogleTests.java"

adb push app/build/intermediates/ndkBuild/debug/obj/local/armeabi-v7a/libtracereduce.so /data/local/tmp/
adb push ./app/build/intermediates/transforms/mergeJniLibs/debug/0/lib/armeabi-v7a/libc++_shared.so /data/local/tmp/
adb push app/build/intermediates/ndkBuild/debug/obj/local/armeabi-v7a/tracereducetest /data/local/tmp/
adb shell chmod 775 /data/local/tmp/tracereducetest
adb shell 'GCOV_PREFIX=/sdcard/gcov_files GCOV_PREFIX_STRIP=17 LD_LIBRARY_PATH=/data/local/tmp /data/local/tmp/tracereducetest'
rm /Users/chaseporter/Repos/testerapp/app/build/intermediates/ndkBuild/debug/obj/local/x86/objs-debug/tracereduce/*.gcda
adb pull /sdcard/gcov_files/. "/Users/chaseporter/Repos/testerapp/app/build/intermediates/ndkBuild/debug/obj/local/x86/objs-debug/tracereduce"
rm -rf /Users/chaseporter/Repos/testerapp/app/build/reports/ndk_coverage/
mkdir /Users/chaseporter/Repos/testerapp/app/build/reports/ndk_coverage/
lcov --directory /Users/chaseporter/Repos/testerapp/app/build/intermediates/ndkBuild/debug/obj/local/x86/objs-debug/tracereduce/ --capture --output-file /Users/chaseporter/Repos/testerapp/app/build/reports/ndk_coverage/app.info
genhtml /Users/chaseporter/Repos/testerapp/app/build/reports/ndk_coverage/app.info -o /Users/chaseporter/Repos/testerapp/app/build/reports/ndk_coverage/