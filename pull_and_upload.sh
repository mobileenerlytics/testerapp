# To pull the most recent trace file from the device:
export MOST_RECENT_DATA_DIR=`adb shell ls -t /sdcard/traces/ | head -1`
export DESTINATION=~/Downloads
adb pull /sdcard/traces/$MOST_RECENT_DATA_DIR $DESTINATION 

 # To upload the profiling trace to the Mobile Enerlytics 
export ZIP_FILE=$DESTINATION/"$MOST_RECENT_DATA_DIR".zip  
cd $DESTINATION
zip -r $ZIP_FILE $MOST_RECENT_DATA_DIR
cd $OLDPWD

export BRANCH="release-8.0"      # identifier bucket for tests
export COMMIT="commit-hash"   # identifier for each test instance
export PKG="com.facebook.katana"  #package name of app under test
export USERNAME=username
export PASSWORD=password

curl -u $USERNAME:$PASSWORD \
   -H 'Cache-Control: no-cache' \
   -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
   -F commit=$COMMIT \
   -F branch=$BRANCH \
   -F pkg=$PKG \
   -F author_name=John \
   -F author_email=jb3@fb.com \
   -F file=@$ZIP_FILE \
# https://tester.mobileenerlytics.com/api/upload/version_energy
