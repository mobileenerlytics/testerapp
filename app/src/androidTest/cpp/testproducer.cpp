#include <sys/types.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <malloc.h>
#include <stdlib.h>
#include <gtest/gtest.h>
#include "bufferpool.h"
#include "traceproducer.h"
#include "../../main/cpp/traceproducer.h"

const char* TEST_PRODUCER_INPUT1 = "abcdefghijklmnopqrstuvwxyz0123456789";
const char* TEST_PRODUCER_RESULT1 = "abcdefghijklmn";

int str_to_pipe(const char *str) {
    int pipe_fd[2];
    pid_t pid;
    assert(pipe(pipe_fd) == 0);

    pid = fork();
    assert(pid >= 0);
    if (pid > 0) {
        return pipe_fd[0];
    } else {
        write(pipe_fd[1], str, strlen(str) + 1);
        close(pipe_fd[1]);
        _exit(0);
    }
}

TEST(ProducerTest, test_producer_fill_buffer) {
    //Setup
    int pipe_fd = str_to_pipe(TEST_PRODUCER_INPUT1);
    BufferNode* node = new BufferNode(15);

    //Test
    FILE *file = fdopen(pipe_fd, "rb"); // use for fread
    producer_fill_buffer(file, node);
    node->buffer[14] = '\0';
    ASSERT_STREQ(TEST_PRODUCER_RESULT1, node->buffer);

    //Teardown
    delete node;
    close(pipe_fd);
}
