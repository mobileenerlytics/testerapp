#include <gtest/gtest.h>
#include <bufferlist.h>

#define BUF_SIZE 8
#define NUM_BUFFERS 8

class BufferListTest : public ::testing::Test {
protected:
    void SetUp() override {
        list = new BufferList();
    }

    BufferList *list;
    void TearDown() override {
        delete list;
    }
};

TEST_F(BufferListTest, AddRemoveTest) {
    BufferNode *node;
    for (int i = 0; i < NUM_BUFFERS; i++) {
        ASSERT_EQ(list->length(), i);
        node = new BufferNode(BUFSIZ);
//        snprintf(node->buffer, sizeof(node->buffer), "%d", i);
        list->push(node);
    }
    for (int i = 0; i < NUM_BUFFERS; i++) {
        ASSERT_EQ(list->length(), NUM_BUFFERS - i);
        node = list->pop();
        delete node;
    }
    ASSERT_EQ(list->length(), 0);
}

TEST_F(BufferListTest, TestFifo) {
    BufferNode** node = new BufferNode*[NUM_BUFFERS];
    for (int i = 0; i < NUM_BUFFERS; i++) {
        node[i] = new BufferNode(BUFSIZ);
//        snprintf(node->buffer, sizeof(node->buffer), "%d", i);
        list->push(node[i]);
    }
    for (int i = 0; i < NUM_BUFFERS; i++) {
        ASSERT_EQ(list->pop(), node[i]);
    }
    delete[] node;
}

//TEST_F(BufferListTest, TestDoublePush) {
//    BufferNode *node = new BufferNode(BUFSIZ);
//    list->push(node);
//    ASSERT_ANY_THROW(list->push(node));
//}
//
//TEST_F(BufferListTest, TestNullPush) {
//    ASSERT_ANY_THROW(list->push(NULL));
//}

