#include <jni.h>
#include <gtest/gtest.h>

extern "C" {
    JNIEXPORT jint JNICALL
    Java_com_mobileenerlytics_eagle_tester_app_GoogleTests_runall(JNIEnv *env, jobject thiz) {
        int argc = 1;
        char* argv = const_cast<char *>("tracereducetest");
        ::testing::InitGoogleTest(&argc, &argv);
        return RUN_ALL_TESTS();
    }
}