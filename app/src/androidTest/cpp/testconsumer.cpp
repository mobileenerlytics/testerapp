#include <gtest/gtest.h>
#include "bufferpool.h"
#include "traceconsumer.h"

#define BUF_SIZE 4096
#define NUM_BUFFERS 4
#define NUM_CONSUMERS 1

class ConsumerTest : public ::testing::Test {
protected:
    void SetUp() override {
        buffer = static_cast<char *>(calloc(BUF_SIZE, sizeof(char)));
        int pos = 0;
        for (int i = 0; i < 4; i++) {
            memcpy(buffer + pos, lines[i], strlen(lines[i]));
            pos += strlen(lines[i]);
        }
    }

    char* STR_1 = const_cast<char *>("sh-10460 (10460) [000] d..3 107811.950861: sched_switch: prev_comm=sh prev_pid=10460 prev_prio=120 prev_state=S ==> next_comm=sdcard next_pid=1547 next_prio=120\n");
    char* STR_2 = const_cast<char *>("irq/163-dwc3-215   (  215) [001] d..3 107811.950947: sched_switch: prev_comm=irq/163-dwc3 prev_pid=215 prev_prio=49 prev_state=S ==> next_comm=swapper/1 next_pid=0 next_prio=120\n");
    char* STR_3 = const_cast<char *>("<idle>-0     (-----) [001] .N.2 107811.950241: cpu_idle: state=4294967295 cpu_id=1\n");
    char* STR_4 = const_cast<char *>("sh-10460 (10460) [000] d..3 107811.952079: sched_switch: prev_comm=sh prev_pid=10460 prev_prio=120 prev_state=S ==> next_comm=sdcard next_pid=1547 next_prio=120\n");
    char *lines[4] = {STR_1, STR_2, STR_3, STR_4};
    char* buffer;
};

TEST_F(ConsumerTest, test_mget_line) {
    int line_start = 0;
    int buffer_index = 0;
    int line_size = 0;
    char line[1024];
    for (int line_num = 0; line_num < 4; line_num++) {
        buffer_index = mget_line(buffer, BUF_SIZE, line_start); // index of new line
        line_size = buffer_index - line_start;
        memcpy(line, buffer + line_start, line_size + 1);
        line[line_size + 1] = 0;
        ASSERT_STREQ(lines[line_num], line);
        line_start = buffer_index + 1;
    }
}

TEST_F(ConsumerTest, test_is_ctx_switch) {
    char line[1024];
    ASSERT_EQ(is_ctx_switch(line, STR_1, strlen(STR_1)), 1);
    ASSERT_EQ(is_ctx_switch(line, STR_2, strlen(STR_2)), 1);
    ASSERT_EQ(is_ctx_switch(line, STR_3, strlen(STR_3)), 0);
    ASSERT_EQ(is_ctx_switch(line, STR_4, strlen(STR_4)), 1);
}

TEST_F(ConsumerTest, test_reduce_line) {
    char line[1024];
    const char *str_1_reduced = "sh-10460 (10460) [000] d..3 107811.950861: sched_switch: prev_pid=10460 next_pid=1547\n";
    const char *str_2_reduced ="irq/163-dwc3-215   (  215) [001] d..3 107811.950947: sched_switch: prev_pid=215 next_pid=0\n";
    const char *str_4_reduced = "sh-10460 (10460) [000] d..3 107811.952079: sched_switch: prev_pid=10460 next_pid=1547\n";

    memcpy(line, STR_1, strlen(STR_1));
    size_t sz = reduce_line(line);
    line[sz] = 0;
    ASSERT_STREQ(line, str_1_reduced);

    memset(line, 0, strlen(STR_1));
    memcpy(line, STR_2, strlen(STR_2));
    sz = reduce_line(line);
    line[sz] = 0;
    ASSERT_STREQ(line, str_2_reduced);

    memset(line, 0, strlen(STR_2));
    memcpy(line, STR_4, strlen(STR_4));
    sz = reduce_line(line);
    line[sz] = 0;
    ASSERT_STREQ(line, str_4_reduced);
}

//int test_consume_buffer(char *buffer) {
//    consumer_start(buffer, BUF_SIZE, "/sdcard/traces/tmp/consumer_output.gz");
//}




