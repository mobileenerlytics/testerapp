#include <gtest/gtest.h>
#include "../../main/cpp/bufferpool.h"

const char* TEST_BUFFERPOOL_INPUT = const_cast<char *>("abcdefghijklmnopqrstuvwxyz0123456789");
const char* TAG = "BufferPoolTest";

void write_to_buffer_pool(BufferPool *pool, const char *input, size_t len);
void read_from_buffer_pool(BufferPool *pool, char *result, size_t times);

TEST(BufferPoolTest, test_buffer_pool_full_length) {
    //Setup
    size_t input_len = strlen(TEST_BUFFERPOOL_INPUT);
    BufferPool *pool = new BufferPool(input_len / 2, 2);
    char *result = (char *) malloc((input_len * sizeof(char)) + 1);
    result[input_len] = '\0';

    //Test
    write_to_buffer_pool(pool, TEST_BUFFERPOOL_INPUT, input_len);
    read_from_buffer_pool(pool, result, input_len);
    ASSERT_STREQ(result, TEST_BUFFERPOOL_INPUT);

    //Teardown
    free(result);
    delete pool;
}

TEST(BufferPoolTest, test_buffer_pool_partial_length) {
    //Setup
    size_t input_len = strlen(TEST_BUFFERPOOL_INPUT);
    BufferPool *pool = new BufferPool(input_len, 1);
    char *result = (char *) malloc((input_len * sizeof(char)) + 1);
    result[input_len] = '\0';

    //Test
    write_to_buffer_pool(pool, TEST_BUFFERPOOL_INPUT, 10);
    read_from_buffer_pool(pool, result, 5);
    write_to_buffer_pool(pool, TEST_BUFFERPOOL_INPUT + 10, 5);
    read_from_buffer_pool(pool, result, 10);
    write_to_buffer_pool(pool, TEST_BUFFERPOOL_INPUT + 15, 21);
    read_from_buffer_pool(pool, result, 5);
    read_from_buffer_pool(pool, result, 16);
    ASSERT_STREQ(result, TEST_BUFFERPOOL_INPUT);

    //Teardown
    free(result);
    delete pool;
}

void write_to_buffer_pool(BufferPool *pool, const char *input, size_t len) {
    for(size_t i = 0; i < len; i += pool->get_buffer_size()) {
        BufferNode *node = pool->get_free_buffer();
        memcpy(node->buffer, input + i, node->capacity);
        pool->mark_full();
    }
}

void read_from_buffer_pool(BufferPool *pool, char *result, size_t len) {
    for(size_t i = 0; i < len; i += pool->get_buffer_size()) {
        BufferNode *node = pool->get_full_buffer();
        memcpy(result + (node->index * node->capacity), node->buffer, node->capacity);
        pool->mark_free(node);
    }
}