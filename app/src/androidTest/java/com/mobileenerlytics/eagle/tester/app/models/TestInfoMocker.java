package com.mobileenerlytics.eagle.tester.app.models;

import android.databinding.ObservableDouble;
import android.databinding.ObservableField;
import android.databinding.ObservableLong;

import org.mockito.Mockito;

import java.lang.reflect.Field;

public class TestInfoMocker {
    TestInfo testInfo = Mockito.mock(TestInfo.class);


    public TestInfoMocker() {
        try {
            Field zipSize = TestInfo.class.getField("uploadSizeMB");
            zipSize.setAccessible(true);
            zipSize.set(testInfo, new ObservableDouble(0.));
            Field zipProgress = TestInfo.class.getField("uploadProgressMB");
            zipProgress.setAccessible(true);
            zipProgress.set(testInfo, new ObservableDouble(0.));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void setTextField(String fieldName, String value) {
        try {
            Field field = TestInfo.class.getField(fieldName);
            field.setAccessible(true);
            field.set(testInfo, new ObservableField<>(value));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setLongField(String fieldName, Long value) {
        try {
            Field field = TestInfo.class.getField(fieldName);
            field.setAccessible(true);
            field.set(testInfo, new ObservableLong(value));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public TestInfo getTestInfo() {
        return testInfo;
    }
}
