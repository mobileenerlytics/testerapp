package com.mobileenerlytics.eagle.tester.app.controllers;

import android.content.SharedPreferences;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableInt;
import android.databinding.ObservableList;

import com.mobileenerlytics.eagle.tester.app.models.TestInfo;
import com.mobileenerlytics.eagle.tester.app.models.TestInfoMocker;
import com.mobileenerlytics.eagle.tester.app.models.TestList;
import com.mobileenerlytics.eagle.tester.app.models.TestStatus;
import com.mobileenerlytics.eagle.tester.app.prefs.SettingsFragment;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Field;

import okhttp3.OkHttpClient;

import static com.mobileenerlytics.eagle.tester.app.tasks.ArchiveTask.zip;
import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.matches;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UploadWorkerTest {
    private TestStatus testStatus = mock(TestStatus.class);
    private SharedPreferences sharedPreferences = mock(SharedPreferences.class);
    @Mock
    TestList testList;
    @Mock
    OkHttpClient okHttpClient;
    @Spy
    @InjectMocks
    UploadThreadPool uploadThreadPool;

    private ObservableList<TestInfo> mockedProcessingTests;
    private TestInfo testInfo1;
    private TestInfo testInfo2;
    private File testDir1;
    private File testDir2;
    private File testZip1;
    private File testZip2;
    private File mbFile;

    public UploadWorkerTest() {
        Mockito.when(sharedPreferences.getString(matches(AuthTask.AUTH_KEY), anyString())).thenReturn("Eagle Tester:password");
        Mockito.when(sharedPreferences.getString(matches(SettingsFragment.URL_KEY), anyString())).thenReturn("https://tester.mobileenerlytics.com");
        Mockito.when(testStatus.isReady()).thenReturn(true);
        Mockito.when(testStatus.setProcessing()).thenReturn(true);
        MockitoAnnotations.initMocks(this);

        testInfo1 = createTestMock("sdcard/traces/test1", "Eagle Tester 1", "commit 1");
        testInfo2 = createTestMock("sdcard/traces/test2", "Eagle Tester 2", "commit 2");

        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                mockedProcessingTests.remove(0);
                return null;
            }
        }).when(testList).remove(any(TestInfo.class));
    }

    @Before
    public void setupTest() throws Exception {
        Object testStatusClass = TestStatus.class;
        Field field = ((Class) testStatusClass).getField("uploadFailed");
        field.setAccessible(true);
        field.set(testStatus, new ObservableInt(0));
        testDir1 = new File("/sdcard/traces/test1");
        testDir1.mkdir();
        String zipPath1 = "/sdcard/traces/test1" + testInfo1.startRealTime.get();
        zip(testDir1.getPath(), zipPath1);
        testZip1 = new File(zipPath1);
        doReturn(testDir1).when(testInfo1).getTestDir();
        doReturn(testZip1).when(testInfo1).zipTest();
        testDir2 = new File("/sdcard/traces/test2");
        testDir2.mkdir();
        String zipPath2 = "/sdcard/traces/test2" + testInfo2.startRealTime.get();
        zip(testDir2.getPath(), zipPath2);
        testZip2 = new File(zipPath2);
        doReturn(testDir2).when(testInfo2).getTestDir();
        doReturn(testZip2).when(testInfo2).zipTest();
        mockedProcessingTests = new ObservableArrayList<TestInfo>() {{
           add(testInfo1);
           add(testInfo2);
        }};
        doReturn(mockedProcessingTests.toArray(new TestInfo[2])).when(testList).getProcessing();
    }

    private TestInfo createTestMock(String logDir, String pkgName, String commitHash) {
        TestInfoMocker testInfoMocker = new TestInfoMocker();
        testInfoMocker.setTextField("logDir", logDir);
        testInfoMocker.setTextField("pkgName", pkgName);
        testInfoMocker.setTextField("commitHash", commitHash);
        testInfoMocker.setLongField("startRealTime", System.currentTimeMillis());
        final TestInfo testInfo = testInfoMocker.getTestInfo();
        doReturn(true).when(testInfo).isStaged();
        doReturn(true).when(testInfo).prepareForUpload();
        doReturn(true).when(testInfo).queueTest();
        return testInfo;
    }

    private RandomAccessFile createMBFile(String path) throws IOException {
        RandomAccessFile file = new RandomAccessFile(path, "rw");
        file.setLength(3 << 20);
        return file;
    }

    @After
    public void cleanUp() {
        if (mbFile != null)
            mbFile.delete();
        if (testDir1 != null)
            testDir1.delete();
        if (testDir2 != null)
            testDir2.delete();
        if (testZip1 != null)
            testZip1.delete();
        if (testZip2 != null)
            testZip2.delete();
    }

    @Test
    public void testUploadSuccess() {
        doReturn(200).when(uploadThreadPool).getHttpResponse(any(OkHttpClient.class), any(UploadThreadPool.UploadRequestBody.class));
        int responseCode= uploadThreadPool.upload(testZip1, testInfo1);
        assertEquals(200, responseCode);
    }

    @Test
    public void testUploadFailure() {
        doReturn(401).when(uploadThreadPool).getHttpResponse(any(OkHttpClient.class), any(UploadThreadPool.UploadRequestBody.class));
        int responseCode = uploadThreadPool.upload(testZip1, testInfo1);
        assertEquals(401, responseCode);
    }

    @Test
    public void testThreadPoolSuccess() {
        doReturn(200).when(uploadThreadPool).upload(any(File.class), any(TestInfo.class));
        boolean terminated = uploadThreadPool.run();
        assertTrue(terminated);
        assertEquals(0, mockedProcessingTests.size());
    }

    @Test
    public void testThreadPoolFailure() {
        doReturn(401).when(uploadThreadPool).upload(any(File.class), any(TestInfo.class));
        boolean terminated = uploadThreadPool.run();
        assertTrue(terminated);
        assertEquals(2, mockedProcessingTests.size());
        assertEquals(401, testStatus.uploadFailed.get());
    }

    @Test
    public void testCancelUpload() throws IOException {
        doReturn(200).when(uploadThreadPool).upload(any(File.class), any(TestInfo.class));
        doAnswer(new Answer<Void> () {
            @Override
            public Void answer(InvocationOnMock invocation) {
                if (testInfo1.uploadFuture != null)
                    testInfo1.uploadFuture.cancel(true);
                return null;
            }
        }).when(testInfo1).getTestDir();
        boolean terminated = uploadThreadPool.run();
        assertTrue(terminated);
        assertEquals(1, mockedProcessingTests.size());
    }

    @Test
    public void testProgressUpdate() {
        mbFile = new File("/sdcard/traces/test1/mbFile");
        try (final RandomAccessFile mbFileToUpload = createMBFile(mbFile.getPath())) {
            testInfo1.uploadSizeMB.set(Math.floor(mbFile.length() / 10000.) / 100);
            doAnswer(new Answer<Integer>() {
                @Override
                public Integer answer(InvocationOnMock invocation) throws IOException {
                    UploadThreadPool.UploadRequestBody body = invocation.getArgument(1);
                    body.testInfo.uploadProgressMB.set(Math.floor(mbFile.length() / 10000.) / 100);
                    return 200;
                }
            }).when(uploadThreadPool).getHttpResponse(any(OkHttpClient.class), any(UploadThreadPool.UploadRequestBody.class));
            uploadThreadPool.upload(mbFile, testInfo1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertTrue(testInfo1.uploadSizeMB.get() > 0);
        assertEquals(testInfo1.uploadSizeMB.get(), testInfo1.uploadProgressMB.get());
    }
}
