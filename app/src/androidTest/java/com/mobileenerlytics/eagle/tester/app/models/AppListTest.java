package com.mobileenerlytics.eagle.tester.app.models;

import android.content.Context;
import android.content.pm.PackageManager;

import com.mobileenerlytics.eagle.tester.app.dagger.AppModule;
import com.mobileenerlytics.eagle.tester.app.dagger.DaggerAppComponent;
import org.junit.runner.RunWith;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.Silent.class)
public class AppListTest {
    AppModule appModule;
    AppList appList;

    @Before
    public void setup() throws PackageManager.NameNotFoundException {
        appModule = new AppModule(new PackageManagerTestProvider().providesPackageManager(), mock(Context.class));
        appList = DaggerAppComponent.builder().appModule(appModule).build().getAppList();
    }

    @Test
    public void testPopulatePackageList() {
        assertEquals(2, appList.appInfos.size());
    }

    @Test
    public void testOrderOfApps() {
        assertEquals("Eagle Tester 1", appList.appInfos.get(0).pkgName.get());
        assertEquals("Eagle Tester 2", appList.appInfos.get(1).pkgName.get());
    }
}