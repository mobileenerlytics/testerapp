package com.mobileenerlytics.eagle.tester.app;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

/**
 * This class will run all the gtests and allow debugging and setting breakpoints
 *
 * To use this make sure "#include $(BUILD_SHARED_LIBRARY)" is uncommented and
 * "include $(BUILD_EXECUTABLE)" is commented for tracereducetest in Android.mk.
 *
 * To get a report of failing tests see rungtests.sh
 */
public class GoogleTests {
    @Before
    public void setup() {
        System.loadLibrary("tracereduce");
        System.loadLibrary("tracereducetest");
    }

    @Test
    public void nativeTest() {
        Assert.assertEquals(0, runall());
    }

    public native int runall();
}
