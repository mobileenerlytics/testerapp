package com.mobileenerlytics.eagle.tester.app.models;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.matches;
import static org.mockito.Mockito.mock;

public class PackageManagerTestProvider {
    PackageManager packageManager = mock(PackageManager.class);
    final ResolveInfo app1 = mock(ResolveInfo.class);
    final ActivityInfo activityInfo1 = mock(ActivityInfo.class);
    final PackageInfo packageInfo1 = mock(PackageInfo.class);
    final ApplicationInfo appInfo1 = mock(ApplicationInfo.class);
    final ResolveInfo app2 = mock(ResolveInfo.class);
    final ActivityInfo activityInfo2= mock(ActivityInfo.class);
    final ApplicationInfo appInfo2 = mock(ApplicationInfo.class);
    final PackageInfo packageInfo2 = mock(PackageInfo.class);
    final List<ResolveInfo> mockedAppsList = new ArrayList<ResolveInfo>() {{
        add(app1);
        add(app2);
        add(app2);
    }};

    final String packageName1 = "Eagle Tester 1";
    final String versionNumber1 = "1.0.0";
    final String packageName2 = "Eagle Tester 2";
    final String versionNumber2 = "1.0.1";

    public PackageManager providesPackageManager() throws PackageManager.NameNotFoundException {
        app1.activityInfo = activityInfo1;
        activityInfo1.packageName = packageName1;
        packageInfo1.versionCode = 50;
        packageInfo1.versionName = versionNumber1;
        app2.activityInfo = activityInfo2;
        activityInfo2.packageName = packageName2;
        packageInfo2.versionCode = 51;
        packageInfo2.versionName = versionNumber2;
        Mockito.when(packageManager.getApplicationInfo(matches(packageName1), anyInt())).thenReturn(appInfo1);
        Mockito.when(packageManager.getApplicationInfo(matches(packageName2), anyInt())).thenReturn(appInfo2);
        Mockito.when(packageManager.queryIntentActivities((Intent) any(), anyInt())).thenReturn(mockedAppsList);
        Mockito.when(packageManager.getApplicationLabel(appInfo1)).thenReturn(packageName1);
        Mockito.when(packageManager.getApplicationLabel(appInfo2)).thenReturn(packageName2);
        Mockito.when(packageManager.getPackageInfo(matches(packageName1), anyInt())).thenReturn(packageInfo1);
        Mockito.when(packageManager.getPackageInfo(matches(packageName2), anyInt())).thenReturn(packageInfo2);
        return packageManager;
    }
}
