package com.mobileenerlytics.eagle.tester.app.controllers;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.nio.charset.StandardCharsets;

import javax.net.ssl.HttpsURLConnection;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.matches;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

public class AuthTaskTest {

    @Mock
    SharedPreferences sharedPreferences;

    @Mock
    Context mockContext;

    @Spy @InjectMocks
    AuthTask authTask;

    @Before
    public void setUpTest() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(mockContext.getSharedPreferences(anyString(), anyInt())).thenReturn(sharedPreferences);
        doNothing().when(authTask).onPostExecute(any(Integer.class));
    }

    @Test
    public void testValidAuthentication() throws Exception {
        String validAuth = Base64.encodeToString("Eagle Tester:password".getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);
        doReturn(200).when(authTask).authConnection(any(HttpsURLConnection.class), matches(validAuth));
        doReturn("Eagle Tester:password").when(sharedPreferences).getString(anyString(), anyString());
        assertEquals(200, (long) authTask.doInBackground());
    }

    @Test
    public void testInvalidAuthentication() throws Exception {
        String inValidAuth = Base64.encodeToString("Invalid Authentication:1234".getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);
        doReturn(401).when(authTask).authConnection(any(HttpsURLConnection.class), matches(inValidAuth));
        when(sharedPreferences.getString(anyString(), anyString())).thenReturn("Invalid Authentication:1234");
        assertEquals(401, (long) authTask.doInBackground());
    }
}
