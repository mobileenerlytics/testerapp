package com.mobileenerlytics.eagle.tester.app.tasks;

import android.widget.ProgressBar;

import com.mobileenerlytics.eagle.tester.app.models.TestInfo;
import com.mobileenerlytics.eagle.tester.app.models.TestInfoMocker;
import com.mobileenerlytics.eagle.tester.app.models.TestList;
import com.mobileenerlytics.eagle.tester.app.models.TestStatus;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;

public class DeleteTaskTest {
    @Mock
    ProgressBar progressBar;
    @Mock
    TestStatus testStatus;
    @Mock
    TestList testList;
    @Spy @InjectMocks
    DeleteTask deleteTask;

    TestInfo testInfo1;
    TestInfo testInfo2;

    File testDir1;
    File testDir2;

    @Before
    public void setUpTest() throws Exception {
        MockitoAnnotations.initMocks(this);

        TestInfoMocker testInfoMocker1 = new TestInfoMocker();
        TestInfoMocker testInfoMocker2 = new TestInfoMocker();
        testInfoMocker1.setTextField("logDir", "sdcard/traces/test1");
        testInfoMocker2.setTextField("logDir", "sdcard/traces/test2");
        testInfo1 = testInfoMocker1.getTestInfo();
        testInfo2 = testInfoMocker1.getTestInfo();
        doReturn(new File("sdcard/traces/test1")).when(testInfo1).getTestDir();
        doReturn(new File("sdcard/traces/test2")).when(testInfo2).getTestDir();
    }

    @After
    public void tearDown() {
        if (testDir1 != null)
            testDir1.delete();
        if (testDir2 != null)
            testDir2.delete();
    }

    @Test
    public void testDeleteExecution() {
        deleteTask.doInBackground(testInfo1, testInfo2);
        assertEquals(2, deleteTask.testsToRemove.size());
    }

}
