package com.mobileenerlytics.eagle.tester.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.filters.LargeTest;
import android.support.test.filters.SdkSuppress;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.util.Log;

import com.mobileenerlytics.eagle.tester.app.controllers.AuthTask;
import com.mobileenerlytics.eagle.tester.app.models.AppInfo;
import com.mobileenerlytics.eagle.tester.app.views.MainActivity;

import org.hamcrest.BaseMatcher;
import org.junit.AfterClass;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runner.RunWith;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.zip.GZIPInputStream;

import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.mobileenerlytics.eagle.tester.app.prefs.PrefUtils.BUGREPORT_KEY;
import static com.mobileenerlytics.eagle.tester.app.prefs.PrefUtils.NETWORK_KEY;
import static com.mobileenerlytics.eagle.tester.app.prefs.PrefUtils.SCREEN_KEY;
import static com.mobileenerlytics.eagle.tester.app.prefs.PrefUtils.STRACE_KEY;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by srainha on 22/05/18.
 * ./gradlew connectedCheck
 * generates Jacoco Report in: app/build/reports/coverage/debug/
 */

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)
@LargeTest
public class LoggersTest {

    private static UiDevice mDevice;
    private static ArrayList<File> TRACE_FILES;
    private static final int UI_DELAY = 1800;
    private static final int START_DELAY = 10000;
    private static final int STOP_DELAY = 10000;
    private static final int BUGREPORT_DELAY = 60000;
    private static final int TEST_DURATION = 5000;
    private static final String DURATION_NAME = "1 minute";
    private static final String TAG = "LoggersTest";
    private static final String TRACE_PATH = "/sdcard/traces/";
    private static final String PACKAGE_NAME = "com.mobileenerlytics.eagle.tester.app";
    private static final HashMap<String, Boolean> loggerPrefs;
    static {
        loggerPrefs = new HashMap<>();
        loggerPrefs.put(SCREEN_KEY, true);
        loggerPrefs.put(STRACE_KEY, true);
        loggerPrefs.put(NETWORK_KEY, false);
        loggerPrefs.put(BUGREPORT_KEY, false);
    }
    private static HashMap<String, Boolean> savedPrefs = new HashMap<>();
    private static HashSet<String> failedTests = new HashSet<>();

    public static class EagleTestMatcher extends BaseMatcher<AppInfo> {
        @Override
        public boolean matches(Object item) {
            return ((AppInfo) item).appName.get().equals("Eagle Tester");
        }

        @Override
        public void describeTo(org.hamcrest.Description description) {

        }
    }

    @Rule
    public TestWatcher watcher = new TestWatcher() {
        @Override
        protected void failed(Throwable e, Description description) {
            failedTests.add(description + " ");
        }

        @Override
        protected void succeeded(Description description) {
        }
    };

    @ClassRule
    public static ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class, true, false);

    @BeforeClass
    public static void setup() throws Exception {
        setupDevice();
        setupEagleTester();
        savePrefs();
        enableLoggers(loggerPrefs);
        runManualTest();
        getTraceFiles();
    }

    private static void setupDevice() throws InterruptedException {
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        mDevice.pressHome();
        final String launcherPackage = mDevice.getLauncherPackageName();
        assertThat(launcherPackage, notNullValue());
        mDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), UI_DELAY);
        Context context = InstrumentationRegistry.getContext();
        final Intent intent = context.getPackageManager().getLaunchIntentForPackage(PACKAGE_NAME);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
        mDevice.wait(Until.hasObject(By.pkg(PACKAGE_NAME).depth(0)), UI_DELAY);
        Thread.sleep(START_DELAY);
    }

    private static void setupEagleTester() throws UiObjectNotFoundException {
        UiObject permissionsNeeded = mDevice.findObject(new UiSelector().resourceId("com.mobileenerlytics.eagle.tester.app:id/storageButton"));
        if (permissionsNeeded.exists()) {
            permissionsNeeded.click();
            mDevice.findObject(new UiSelector().resourceId("com.android.packageinstaller:id/permission_allow_button")).click();
            mDevice.findObject(new UiSelector().resourceId("com.mobileenerlytics.eagle.tester.app:id/configButton")).click();
        }
        Espresso.onView(withId(R.id.packageSpinner)).perform(click());
        Espresso.onData(allOf(is(instanceOf(AppInfo.class)), new EagleTestMatcher())).perform(click());
        Espresso.onView(withId(R.id.durationSpinner)).perform(click());
        Espresso.onData(allOf(is(instanceOf(String.class)), is(DURATION_NAME))).perform(click());
    }

    private static void savePrefs() {
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        for (String pref : loggerPrefs.keySet()) {
            savedPrefs.put(pref, sharedPreferences.getBoolean(pref, true));
        }
    }

    private static void enableLoggers(HashMap<String, Boolean> loggerMap) {
        for (String pref : loggerMap.keySet()) {
            //Use only with toggled loggers (screen, bugreport, strace, tcpdump)
            Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(pref, loggerMap.get(pref));
            editor.commit();
        }
    }

    private static void runManualTest() {
        Log.d(TAG, "Testing start and stop logging");
        Espresso.onView(withId(R.id.mainButton)).perform(click());
        SystemClock.sleep(START_DELAY);
        if (loggerPrefs.get(NETWORK_KEY)) makeNetworkRequest();
        SystemClock.sleep(START_DELAY + TEST_DURATION);
        Espresso.onView(withId(R.id.mainButton)).perform(click());
        SystemClock.sleep(STOP_DELAY);
        if (loggerPrefs.get(BUGREPORT_KEY)) SystemClock.sleep(BUGREPORT_DELAY);
    }

    private static void getTraceFiles() {
        File[] traceFolders = (new File(TRACE_PATH)).listFiles();
        Arrays.sort(traceFolders, new Comparator<File>() {
            public int compare(File f1, File f2) {
                return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
            }
        });
        TRACE_FILES = new ArrayList<>(Arrays.asList(traceFolders[traceFolders.length - 1].listFiles()));
    }

    @AfterClass
    public static void tearDown() {
        enableLoggers(savedPrefs);
        if (failedTests.isEmpty()) {
            Log.d(TAG, "=== All Tests Passed, Deleting Logs ===");
            mActivityRule.launchActivity(new Intent(Intent.ACTION_MAIN));
            SystemClock.sleep(UI_DELAY);
            Espresso.onView(withId(R.id.selectAll)).perform(click());
            Espresso.onView(withId(R.id.deleteFiles)).perform(click());
        } else {
            for (String failedtest : failedTests) {
                Log.d(TAG, "=== " + failedtest + "failed ===");
            }
        }
    }

    private File getTraceFile(String filename) {
        for (File file : TRACE_FILES) {
            if (file.getName().equals(filename)) return file;
        }
        return null;
    }

    private File checkFile(String filename) {
        File file = getTraceFile(filename);
        assertTrue(filename,file != null);
        assertTrue(file.getAbsolutePath(), file.length() != 0);
        return file;
    }

    private void readFileForPatterns(File file, HashSet<String> linePatterns) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                for (String linePattern : linePatterns) {
                    if (line.matches(linePattern)) {
                        linePatterns.remove(linePattern);
                        break;
                    }
                }
                if (linePatterns.size() == 0) break;
            }
        } catch (Exception e) {
            fail(e.toString());
        }
    }

    private void checkFileContents(File file, String... patterns) {
        HashSet<String> linePatterns = new HashSet<>(Arrays.asList(patterns));
        readFileForPatterns(file, linePatterns);
        for (String linePattern : linePatterns) {
            Log.e(TAG, "Pattern not found: " + linePattern);
        }
        assertTrue(linePatterns.size() == 0);
    }

    private void checkFilesContents(ArrayList<File> files, String... patterns) {
        HashSet<String> linePatterns = new HashSet<>(Arrays.asList(patterns));
        for (File file : files) {
            readFileForPatterns(file, linePatterns);
            if (linePatterns.size() == 0) break;
        }
        for (String linePattern : linePatterns) {
            Log.e(TAG, "Pattern not found: " + linePattern);
        }
        assertTrue(linePatterns.size() == 0);
    }

    private static void makeNetworkRequest() {
        try {
            Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
            AuthTask authTask = ((MainApplication)context.getApplicationContext()).getAppComponent().getAuthTask();
            int code = authTask.execute().get();
            Log.d(TAG, "NetworkRequest: " + code);
        } catch (Exception e) {
            Log.e("NetworkRequest", e.toString());
        }
    }

    @Test
    public void testPsLogger() {
        Log.d(TAG, "=== Starting PsLogger test ===");
        String filename = "ps_out";
        File file = checkFile(filename);
        // root      154   2     kgsl_devfreq_wqM
        String psLoggerPattern = ".*mobileenerlytics.*";
        checkFileContents(file, psLoggerPattern);
        Log.d(TAG, "=== Ending PsLogger test ===");
    }

    @Test
    public void testScreenLogger() {
        Assume.assumeTrue("=== Screen Logging Disabled, Skipping Test ===", loggerPrefs.get(SCREEN_KEY));
        Log.d(TAG, "=== Starting Screen Logging test ===");
        checkFile("screen0.mp4");
        Log.d(TAG, "=== Ending Screen Logging test ===");
    }

    @Test
    public void testBuildProp() {
        Log.d(TAG, "=== Starting build.prop test ===");
        File file = checkFile("build.prop");
        checkFileContents(file, "cpu.cores=.+");
        checkFileContents(file, "ro.product.device=.+");
        checkFileContents(file, "tester_logname=.+");
        Log.d(TAG, "=== Ending build.prop test ===");
    }

    @Test
    public void testStrace() {
        Assume.assumeTrue("=== Strace Logging Disabled, Skipping Test ===", loggerPrefs.get(STRACE_KEY));
        Log.d(TAG, "=== Starting strace test ===");
        File file = checkFile("strace");
        // 22240 17:08:11.318556 ioctl(11, _IOC(_IOC_READ|_IOC_WRITE, 0x62, 0x01, 0x18) <unfinished ...>
        String stracePattern = "\\d+\\s+\\d{2}:\\d{2}:\\d{2}\\.\\d+\\s+.*";
        checkFileContents(file, stracePattern);
        Log.d(TAG, "=== Ending strace test ===");
    }

    @Test
    public void testLogcat() {
        Log.d(TAG, "=== Starting logcat test ===");
        String filename = "logcat_out";
        File file = checkFile(filename);
        checkFileContents(file, ".*Started time.*");
        checkFileContents(file, ".*Stopping time.*");
        Log.d(TAG, "=== Ending logcat test ===");
    }


    @Test
    public void testBatteryStats() {
        Log.d(TAG, "=== Starting batterystats test ===");
        String filename = "batterystats";
        File file = checkFile(filename);
//        String batteryStatsPattern = "";
//        checkFileContents(file, batteryStatsPattern);
        Log.d(TAG, "=== Ending batterystats test ===");
    }

    @Test
    public void testTcpDump() {
        Assume.assumeTrue("=== Network Logging Disabled, Skipping Test ===", loggerPrefs.get(NETWORK_KEY));
        Log.d(TAG, "=== Starting logcat test ===");
        checkFile("tcp");
        checkFile("tcp6");
        checkFile("udp");
        checkFile("udp6");
        makeNetworkRequest();
        checkFile("capture.pcap");
        Log.d(TAG, "=== Ending logcat test ===");
    }

    @Test
    public void testBugReport() {
        Assume.assumeTrue("=== BugReport Disabled, Skipping Test ===", loggerPrefs.get(BUGREPORT_KEY));
        Log.d(TAG, "=== Starting bugreport test ===");
        String filename = "bugreport.*(zip|txt)";
        for (File file : TRACE_FILES) {
            if (file.getName().matches(filename)) {
                checkFile(file.getName());
                filename = "";
                break;
            }
        }
        if (!filename.equals("")) {
            Log.d(TAG, "Could not locate BugReport Zip file");
        }
        assertTrue(filename.equals(""));
        Log.d(TAG, "=== Ending bugreport test ===");
    }

    private String gunzipFile(File file) {
        String zipName = file.getName();
        String atraceCount = zipName.replaceAll("[^0-9]", "");
        String uncPath = "/sdcard/traces/atrace_" + atraceCount;
        byte[] buffer = new byte[1024];
        try {
            GZIPInputStream gzis = new GZIPInputStream(new FileInputStream(file));
            FileOutputStream out = new FileOutputStream(uncPath);
            int len;
            while ((len = gzis.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }
            gzis.close();
            out.close();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            return null;
        }
        return uncPath;
    }

    @Test
    public void testAtrace() {
        Log.d(TAG, "=== Starting atrace test ===");
        ArrayList<File> atraceFiles = new ArrayList<>();
        String[] atraceChecks = new String[]
                {       ".*tracing_mark_write: trace_event_clock_sync: parent_ts.*",
                        ".*tracing_mark_write: trace_event_clock_sync: realtime_ts.*",
                        ".*sched_switch.*"
                };
        for (File file : TRACE_FILES) {
            if (file.getName().contains("atrace")) {
                String uncPath = gunzipFile(file);
                atraceFiles.add(new File(uncPath));
            }
        }
        checkFilesContents(atraceFiles, atraceChecks);
        for (File file : atraceFiles)
            file.delete();
        Log.d(TAG, "=== Ending atrace test ===");
    }
}
