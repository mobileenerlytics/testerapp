package com.mobileenerlytics.eagle.tester.app.models;

import android.databinding.ObservableInt;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Field;

import static com.mobileenerlytics.eagle.tester.app.models.TestInfo.STATUS.CHECKED;
import static com.mobileenerlytics.eagle.tester.app.models.TestInfo.STATUS.UNCHECKED;
import static org.junit.Assert.assertEquals;

public class TestListTest {
    @Mock
    TestInfo testInfo1;
    @Mock
    TestInfo testInfo2;
    @Spy
    TestList testList;

    File buildProp = new File("/sdcard/traces/sampleTest/build.prop");

    @Before
    public void setupTest() throws Exception{
        MockitoAnnotations.initMocks(this);
        Field testState = TestInfo.class.getField("state");
        testState.setAccessible(true);
        testState.set(testInfo1, new ObservableInt(0));
        testState.set(testInfo2, new ObservableInt(0));
        buildProp.getParentFile().mkdir();
        buildProp.createNewFile();
        buildProp.setWritable(true);
        FileWriter writer = new FileWriter(buildProp);
        writer.write("packageName=Eagle Tester\ntestName=Eagle Tester App\ncommitHash=sampleCommit\n");
        writer.close();
    }

    @After
    public void cleanUp() {
        File parentFolder = buildProp.getParentFile();
        buildProp.delete();
        parentFolder.delete();
    }

    @Test
    public void testCreateTestInfo() {
        TestInfo testInfo = testList.createTestInfo(buildProp.getParentFile());
        assertEquals(testInfo.pkgName.get(), "Eagle Tester");
        assertEquals(testInfo.testName.get(), "Eagle Tester App");
        assertEquals(testInfo.commitHash.get(), "sampleCommit");
    }

    @Test
    public void testListAddsOnlyNewTestInfos() {
        testList.add(testInfo1);
        assertEquals(1, testList.testInfos.size());
        testList.add(testInfo2);
        assertEquals(2, testList.testInfos.size());
        testList.add(testInfo1);
        assertEquals(2, testList.testInfos.size());
    }

    @Test
    public void checkingInfosAddsAndRemovesOnObservable() {
        testList.add(testInfo1);
        testInfo1.state.set(CHECKED);
        assertEquals(1, testList.checkedTestInfos.size());
        testInfo1.state.set(UNCHECKED);
        assertEquals(0, testList.checkedTestInfos.size());
    }
}
