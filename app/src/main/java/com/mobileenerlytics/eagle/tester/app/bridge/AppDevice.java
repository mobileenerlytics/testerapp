package com.mobileenerlytics.eagle.tester.app.bridge;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.Log;

import com.mobileenerlytics.eprof.logger.bridge.ClientBridge;
import com.mobileenerlytics.eprof.logger.bridge.IDeviceBridge;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class AppDevice implements IDeviceBridge {
    private static final long MAX_WAIT_MS =  10*60*1000; // 30 minutes
    final Context mContext;
    final ActivityManager mAm;
    final String mScriptFolder, mLibFolder;
    private static final String TAG = "AppDevice";

    public AppDevice(Context context) {
        mContext = context;
        mAm = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
        mScriptFolder = context.getCacheDir().getAbsolutePath() + "/scripts/";
        mLibFolder = context.getApplicationInfo().nativeLibraryDir;
    }

    @Override
    public String getName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return model.toUpperCase(Locale.getDefault());
        }
        return manufacturer.toUpperCase(Locale.getDefault()) + " " + model;
    }

//    @Override
//    public void pushFile(String remotePath, String s1) throws Exception {
//        // No-op
//    }

//    @Override
//    public void pullFile(String fromPath, String toPath) throws Exception {
//        File fromFile = new File(fromPath);
//        File toFile = new File(toPath);
//        boolean mvStatus = fromFile.renameTo(toFile);
//        if(!mvStatus) {
//            String cmd = String.format("mv %s %s", fromPath, toPath);
//            executeShellCommand(cmd, 1, TimeUnit.MINUTES);
//        }
//    }

    @Override
    public Future<String> executeShellCommand(String cmd, long time, @Nullable TimeUnit timeUnit)
            throws Exception, IOException {
        Log.i(TAG, cmd);
        Process proc = new ProcessBuilder().command("su", "-c", cmd).start();
        long millis = MAX_WAIT_MS;
        if (timeUnit != null)
            millis = timeUnit.toMillis(time);
        return new AppFuture(proc, millis);
    }

    // class WaitRunnable implements  Runnable {
    //     Process mProcess;
    //     CountDownLatch mLatch;

    //     WaitRunnable(Process process, CountDownLatch latch) {
    //         mProcess = process;
    //         mLatch = latch;
    //     }

    //     @Override
    //     public void run() {
    //         try {
    //             mProcess.wait();
    //         } catch (InterruptedException e) {
    //             e.printStackTrace();
    //         }
    //         mProcess.destroy();
    //         mLatch.countDown();
    //     }
    // }

    // // TODO: Move this to handle a list of waitThreads using Delta Queue implementation
    // class InterruptRunnable implements Runnable {
    //     private final Thread mWaitThread;
    //     private final long mWaitMs;

    //     InterruptRunnable(Thread waitThread, long waitMs) {
    //         mWaitThread = waitThread;
    //         mWaitMs = waitMs;
    //     }

    //     @Override
    //     public void run() {
    //         try {
    //             Thread.sleep(mWaitMs);
    //         } catch (InterruptedException e) {
    //             e.printStackTrace();
    //         }
    //         mWaitThread.interrupt();
    //     }
    // }

    // @Override
    // public CountDownLatch executeShellCommandToFile(String file, String cmd) throws Exception, IOException {
    //     return executeShellCommandToFile(file, cmd, 0, null);
    // }

    // @Override
    // public CountDownLatch executeShellCommandToFile(String file, String cmd,
    //                                                 @Nullable long time, @Nullable TimeUnit timeUnit)
    //         throws Exception, IOException {

    //     long millis = MAX_WAIT_MS;
    //     if (timeUnit != null)
    //         millis = timeUnit.toMillis(time);
    //     final Process process = execShellCommand(cmd);
    //     final CountDownLatch latch = new CountDownLatch(1);
    //     Thread waitThread = new Thread(new WaitRunnable(process, latch));
    //     waitThread.start();
    //     new Thread(new InterruptRunnable(waitThread, millis));
    //     return latch;
    // }

    // private Process execShellCommand (String cmd) throws Exception {
    //     Log.i(TAG, cmd);
    //     return new ProcessBuilder().command("su", "-c", cmd).start();
    // }

    @Override
    public String getProperty(String prop) {
        try {
            Future<String> future = executeShellCommand("getprop " + prop, 10, TimeUnit.SECONDS);
            return future.get().trim();
        } catch (Exception e) {
            Log.w(TAG, "Error getting property " + prop);
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ClientBridge[] getClients() {
        List<ClientBridge> apps = new LinkedList<>();
        for(ActivityManager.RunningAppProcessInfo appProcessInfo: mAm.getRunningAppProcesses()) {
            apps.add(AppClient.getClient(appProcessInfo));
        }
        return apps.toArray(new AppClient[apps.size()]);
    }

    @Override
    public void pushScript(URL url) throws Exception {
        String scriptName = FilenameUtils.getName(url.getPath());
        String scriptAbsPath = mScriptFolder + "/" + scriptName;
        File scriptFile = new File(scriptAbsPath);
        FileUtils.copyURLToFile(url, scriptFile);
        Future<String> chmodFuture = executeShellCommand("chmod +x " + scriptAbsPath, 10, TimeUnit.SECONDS);
        chmodFuture.get();  // Wait to finish
    }

    @Override
    public Future<String> runScript(String scriptName, String[] args)
            throws Exception {
        StringBuilder cmdBuilder = new StringBuilder();
        cmdBuilder.append("LD_LIBRARY_PATH=" + mLibFolder + ' ');
        String scriptAbsPath = mScriptFolder + "/" + scriptName;
        cmdBuilder.append(scriptAbsPath);
        if(args != null) {
            for(String arg: args) {
                cmdBuilder.append(' ');
                cmdBuilder.append(arg);
            }
        }
        return executeShellCommand(cmdBuilder.toString(), 1, TimeUnit.MINUTES);
    }

    @Override
    public int getNumCores() {
        if (Build.VERSION.SDK_INT >= 17) {
            return Runtime.getRuntime().availableProcessors();
        } else {
            return getNumCoresOldPhones();
        }
    }

    @Override
    public int getAPILevel() {
        return Build.VERSION.SDK_INT;
    }

    @Override
    public String getVersionRelease() {
        return Build.VERSION.RELEASE;
    }

    private int getNumCoresOldPhones() {
        //Private Class to display only CPU devices in the directory listing
        class CpuFilter implements FileFilter {
            @Override
            public boolean accept(File pathname) {
                //Check if filename is "cpu", followed by a single digit number
                if(Pattern.matches("cpu[0-9]+", pathname.getName())) {
                    return true;
                }
                return false;
            }
        }

        try {
            //Get directory containing CPU info
            File dir = new File("/sys/devices/system/cpu/");
            //Filter to only list the devices we care about
            File[] files = dir.listFiles(new CpuFilter());
            //Return the number of cores (virtual CPU devices)
            return files.length;
        } catch(Exception e) {
            //Default to return 2 core
            return 2;
        }
    }

    public String getCachePath() {
        return mContext.getCacheDir().getAbsolutePath();
    }

}
