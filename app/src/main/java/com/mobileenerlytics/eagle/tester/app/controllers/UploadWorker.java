package com.mobileenerlytics.eagle.tester.app.controllers;

import android.app.job.JobService;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.mobileenerlytics.eagle.tester.app.MainApplication;

import javax.inject.Inject;

import androidx.work.Worker;
import androidx.work.WorkerParameters;

/**
 * UploadWorker will be scheduled by WorkManager in TestList to run after a user clicks the upload button.
 * Schedule it with:
 *         Constraints uploadConstraints = new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build();
 *         OneTimeWorkRequest uploadWork = new OneTimeWorkRequest.Builder(UploadWorker.class).setConstraints(uploadConstraints).build();
 *         WorkManager.getInstance().enqueue(uploadWork);
 *
 * When run will kick off an UploadThreadPool which will upload all CHECKED TestInfo's. Waits until UploadThreadPool has finished before
 * declaring success. If interrupted, schedules itself to run again in the future.
 *
 * @author Chase Porter
 * @since 2019-04-08
 */
public class UploadWorker extends Worker {
    private final String TAG = JobService.class.getSimpleName();

    public static final int UPLOAD_JOB_ID = 0;
    public static final String UPLOAD_INTENT = "com.mobileenerlytics.UPLOAD";

    @Inject
    UploadThreadPool uploadThreadPool;

    public UploadWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        ((MainApplication)getApplicationContext()).getAppComponent().inject(this);
    }

    @NonNull
    @Override
    public Result doWork() {
        Log.i(TAG, "Upload Worker Started.");
        boolean terminated = uploadThreadPool.run();
        if (terminated) {
            Log.i(TAG, "Uploading completed.");
            return Result.success();
        } else {
            Log.i(TAG, "Uploading failed or timed out. Job rescheduled.");
            return Result.retry();
        }
    }

    @Override
    public void onStopped() {
        if (!uploadThreadPool.pool.isShutdown())
            uploadThreadPool.pool.shutdownNow();
        if (uploadThreadPool.pool.isShutdown())
           Log.i(TAG, "UploadThreadPool shutdown succesfully.");
        else
            Log.e(TAG, "Attempt to stop UploadThreadPool failed.");

    }
}
