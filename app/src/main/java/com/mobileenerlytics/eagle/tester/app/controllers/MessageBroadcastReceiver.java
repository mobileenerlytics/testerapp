package com.mobileenerlytics.eagle.tester.app.controllers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.Observable;
import android.util.Log;

import com.mobileenerlytics.eagle.tester.app.MainApplication;
import com.mobileenerlytics.eagle.tester.app.models.TestStatus;
import com.mobileenerlytics.eagle.tester.app.util.MyServiceConnection;
import com.mobileenerlytics.eprof.logger.EprofLoggerMain;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

/**
 * Class for sending launch intents to start logging All settings are disabled
 * by default and must be enabled with a corresponding flag.
 *
 * First launch the service:
 * adb shell am startservice com.mobileenerlytics.eagle.tester.app/.MessageService;
 *
 * or launch the app:
 * adb shell am start -W -n com.mobileenerlytics.eagle.tester.app/.views.MainActivity;
 *
 * Start command:
 * adb shell am broadcast -a com.mobileenerlytics.eagle.tester.app.START_PROFILING -n com.mobileenerlytics.eagle.tester.app/.controllers.MessageBroadcastReceiver \
 * -e packageName <AUT-packageName> -e testName <nameOfTest> -e commitHash <commitHash> \
 * --esn launchAUT \
 * --esn keepawake \
 * --esn strace \
 * --esn network \
 * --esn screen \
 * --esn current_sensor \
 *
 * Stop command:
 * adb shell am broadcast -a com.mobileenerlytics.eagle.tester.app.STOP_PROFILING -n com.mobileenerlytics.eagle.tester.app/.controllers.MessageBroadcastReceiver
 *
 */
public class MessageBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "MsgBroadcastReceiver";

    @Inject
    MessageHelper messageHelper;

    @Inject
    volatile TestStatus testStatus;

    @Inject
    MyServiceConnection myServiceConnection;

    @Inject
    PackageManager packageManager;

    /** Useful for sending start and stop logging commands directly from adb. */
    @Override
    public void onReceive(Context context, final Intent intent) {
        ((MainApplication) context.getApplicationContext()).getAppComponent().inject(this);

        if(! myServiceConnection.isBound()) {
            Log.e(TAG, "Failed to process broadcast. Please launch the app before sending a broadcast.");
            return;
        }

        final CountDownLatch latch = new CountDownLatch(1);
        Observable.OnPropertyChangedCallback callback = null;
        if(intent.getAction().endsWith(".app.START_PROFILING")) {
            if (!testStatus.isReady()) {
                Log.e(TAG, "Logging already in progress. Cannot start profiling.");
                return;
            } else {
                int bitmap = 0; //blacklist
                if (!intent.hasExtra("strace"))
                    bitmap |= EprofLoggerMain.LoggedComponent.STRACE.bitmap;
                if (!intent.hasExtra("bugreport"))
                    bitmap |= EprofLoggerMain.LoggedComponent.HISTORIAN.bitmap;
                if (!intent.hasExtra("network"))
                    bitmap |= EprofLoggerMain.LoggedComponent.NET.bitmap;
                if (!intent.hasExtra("screen"))
                    bitmap |= EprofLoggerMain.LoggedComponent.SCREEN_VIDEO.bitmap;
                if (!intent.hasExtra("current_sensor"))
                    bitmap |= EprofLoggerMain.LoggedComponent.CURRENT_SENSOR.bitmap;

                String pkgName = intent.getStringExtra("packageName");
                String commitHash = intent.getStringExtra("commitHash");
                String testName = intent.getStringExtra("testName");
                boolean keepAwake = intent.hasExtra("keepawake");

                if (commitHash == null) {
                    Log.e(TAG, "Failed to start logging. Must pass in a commitHash when starting a new test");
                    return;
                }
                if (pkgName == null) {
                    Log.e(TAG, "Failed to start logging. Must pass in a package name when starting a new test");
                    return;
                }
                callback = new Observable.OnPropertyChangedCallback() {
                    @Override
                    public void onPropertyChanged(Observable sender, int propertyId) {
                        if (testStatus.isLogging())
                            latch.countDown();
                    }
                };
                if (intent.hasExtra("launchAUT")) {
                    Intent launchIntent = packageManager.getLaunchIntentForPackage(pkgName);
                    (context.getApplicationContext()).startActivity(launchIntent);
                }

                testStatus.addOnPropertyChangedCallback(callback);
                messageHelper.startLogging(bitmap, pkgName, testName, commitHash, keepAwake);
            }
        } else if (intent.getAction().endsWith(".app.STOP_PROFILING")){
            if (!testStatus.isLogging()) {
                Log.e(TAG, "No logging currently in progress. Cannot stop profiling.");
                return;
            } else {
                callback = new Observable.OnPropertyChangedCallback() {
                    @Override
                    public void onPropertyChanged(Observable sender, int propertyId) {
                        if (testStatus.isReady())
                            latch.countDown();
                    }
                };
                testStatus.addOnPropertyChangedCallback(callback);
                messageHelper.stopLogging();
            }
        }
        if (callback != null) {
            try {
                latch.await(10, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            testStatus.removeOnPropertyChangedCallback(callback);
        }
    }
}
