package com.mobileenerlytics.eagle.tester.app.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableDouble;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.databinding.ObservableLong;
import android.os.Environment;
import android.support.annotation.IntDef;
import android.util.Log;

import com.mobileenerlytics.eagle.tester.app.views.ViewAdapter;

import org.apache.commons.lang3.time.DurationFormatUtils;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.Future;

import static com.mobileenerlytics.eagle.tester.app.tasks.ArchiveTask.zip;

/**
 * Class that holds information about a conducted test:
 *     testName, packageName of the tested app, duration of test, startTime,the directory it's stored in, how large its
 *     zip file is, and how much of that file has been uploaded.
 *
 * Has an internal state that manages whether the test has been checked, staged to upload, is queued by UploadThreadPool,
 * is zipping, or is uploading.
 *
 * @author Chase Porter
 * @since 2019-04-08
 */
public class TestInfo extends BaseObservable {
    private final String TAG = TestInfo.class.getSimpleName();
    public final ObservableInt state = new ObservableInt(STATUS.UNCHECKED);
    public final ObservableField<String> testName = new ObservableField<>("");
    public final ObservableField<String> pkgName = new ObservableField<>("");
    public final ObservableField<String> duration = new ObservableField<>("");
    public final ObservableLong startRealTime = new ObservableLong(0);
    public final ObservableField<String> logDir = new ObservableField<>("");
    public final ObservableField<String> commitHash = new ObservableField<>("");
    public final ObservableDouble uploadSizeMB = new ObservableDouble(0.0);
    public final ObservableDouble uploadProgressMB = new ObservableDouble(0.0);
    public final String traceDir;
    public Future uploadFuture;

    /** Test moves from UNCHECKED when first created, to CHECKED by user, to STAGED when user chooses to upload, to QUEUED when worker
     * is run and thread has been made for it, to UPLOADING when OkHttp Request has been made. When cancelled, test goes back to
     * UNCHECKED. If interrupted because of network conditions it goes to STAGED and worker is rescheduled to run. */
    @IntDef({STATUS.UNCHECKED, STATUS.CHECKED, STATUS.STAGED, STATUS.QUEUED, STATUS.COMPRESSING, STATUS.UPLOADING})
    @Retention(RetentionPolicy.SOURCE)
    public @interface STATUS {
        int UNCHECKED = 0;
        int CHECKED = 1;
        int STAGED = 2;
        int QUEUED = 3;
        int COMPRESSING = 4;
        int UPLOADING = 5;
    }

    public TestInfo(String pkgName, String commitHash, String testName) {
        super();
        this.pkgName.set(pkgName);
        this.commitHash.set(commitHash);
        this.testName.set(testName);
        String externalStorage = Environment.getExternalStorageDirectory().getAbsolutePath();
        this.traceDir = externalStorage + "/traces/";
        setState(STATUS.UNCHECKED);
    }


    /**
     * Method to set the STATUS of a TestInfo. Should only be called by TestInfo from a method like
     * StageTest.
     *
     * @param newStatus - STATUS of TestInfo to be set to.
     */
    private synchronized void setState(@STATUS int newStatus) {
        // This is to break the race condition in cancelling upload.
        // When user cancels upload, the state is set to unchecked and then the upload
        // thread is interrupted. But the interrupted upload thread might continue for
        // a while until an API is called that throws an InterruptedException. Hence,
        // the upload thread might try to set the state to COMPRESSING or UPLOADING
        // and the UI might show a progress bar even after user has cancelled uploading.
        if(isUnchecked())
            if(newStatus == STATUS.COMPRESSING || newStatus == STATUS.UPLOADING)
                return;
        this.state.set(newStatus);
        // Need this for now. Ui bound to state functions atm.
        notifyChange();
    }

    /** If TestInfo is UNCHECKED, move it to CHECKED, and vice versa. */
    public synchronized boolean checkTest() {
        if (isUnchecked()) {
            setState(STATUS.CHECKED);
            return true;
        } else if (isChecked()) {
            setState(STATUS.UNCHECKED);
            return true;
        }
        Log.e(TAG, "Attempting to check TestInfo which cannot be checked.");
        return false;
    }

    /** If TestInfo is CHECKED, QUEUED, COMPRESSING, or UPLOADING, move it to STAGED. */
    synchronized boolean stageTest() {
        if (isUnchecked()) {
            Log.e(TAG, "Attempting to stage an UnChecked Test.");
            return false;
        }
        setState(STATUS.STAGED);
        uploadProgressMB.set(0.);
        ViewAdapter.updateTest(this);
        return true;
    }

    /** Move TestInfo to UNCHECKED. */
    public synchronized boolean unStageTest() {
        setState(STATUS.UNCHECKED);
        return true;
    }

    /** If TestInfo is STAGED, QUEUED, COMPRESSING, or UPLOADING, move it to UNCHECKED and cancel its future. */
    public boolean cancelUpload() {
        if (isUnchecked() || isChecked()) {
            Log.e(TAG, "Attempting to cancel a Test not queued to upload.");
            return false;
        }
        // We’re setting the status to unchecked and then canceling the uploadFuture. We set it to unchecked first so that the
        // UI can update immediately. This is safe because the STATUS is still on WAITING and user will not be able to do more
        // operations on this TestInfo anyways until the status become READY. The STATUS will only become READY after upload has
        // indeed canceled.
        setState(STATUS.UNCHECKED);
        if (uploadFuture != null) {
            uploadFuture.cancel(true);
        }
        uploadSizeMB.set(0.);
        uploadProgressMB.set(0.);
        ViewAdapter.updateTest(this);
        return true;
    }

    /** If TestInfo is STAGED move it to QUEUED */
    public synchronized boolean queueTest() {
        if (!isStaged()) {
            Log.e(TAG, "Attempting to queue a Test not staged.");
            return false;
        }
        setState(STATUS.QUEUED);
        return true;
    }

    /** If TestInfo is QUEUED move it to COMPRESSING */
    synchronized boolean prepareForCompress() {
        if (!isQueued()) {
            Log.e(TAG, "Attempting to compress a Test not queued");
            return false;
        }
        setState(STATUS.COMPRESSING);
        return true;
    }

    /** Function to get TestInfo's directory */
    public File getTestDir() throws IOException {
        if (logDir.get().equals("")) {
            throw new IOException("Test directory name not set.");
        }
        return new File(logDir.get());
    }

    /** Function to compress a TestInfo into zip for upload */
    public File zipTest() throws IOException {
        if (!prepareForCompress())
            throw new IOException("Unable to zip file at " + logDir.get());
        String sourcePath = logDir.get();
        String zipPath = traceDir + "eagle-" + startRealTime.get() + ".zip";
        boolean zipped = zip(sourcePath, zipPath);
        if (!zipped)
            throw new IOException("Unable to zip file at " + logDir.get());
        File zippedFile = new File(zipPath);
        uploadSizeMB.set(Math.floor(zippedFile.length() / 10000.) / 100);
        return zippedFile;
    }

    /** If TestInfo is COMPRESSING move to UPLOADING */
    public synchronized boolean prepareForUpload() {
        if (!isCompressing()) {
            Log.e(TAG, "Attempting to upload a test that was not compressed.");
            return false;
        }
        setState(STATUS.UPLOADING);
        return true;
    }

    /** Set the duration of TestInfo */
    public void setDuration(long durationInMs) {
        String formattedDuration = DurationFormatUtils.formatDuration(durationInMs, "mm'm' ss's'", false);
        duration.set(formattedDuration);
    }

    @Bindable
    public boolean isUnchecked() {
        return state.get() == STATUS.UNCHECKED;
    }

    @Bindable
    public boolean isChecked() {
        return state.get() == STATUS.CHECKED;
    }

    @Bindable
    public boolean isStaged() {
        return state.get() == STATUS.STAGED;
    }

    @Bindable
    public boolean isQueued() {
        return state.get() == STATUS.QUEUED;
    }

    @Bindable
    public boolean isCompressing() {
        return state.get() == STATUS.COMPRESSING;
    }

    @Bindable
    public boolean isUploading() {
        return state.get() == STATUS.UPLOADING;
    }

}
