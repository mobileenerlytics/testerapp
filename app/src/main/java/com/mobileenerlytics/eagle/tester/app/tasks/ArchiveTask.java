package com.mobileenerlytics.eagle.tester.app.tasks;

import android.util.Log;

import com.mobileenerlytics.eagle.tester.app.models.TestList;
import com.mobileenerlytics.eagle.tester.app.models.TestStatus;
import com.mobileenerlytics.eagle.tester.app.util.ZipUtil;

import java.io.FileOutputStream;
import java.util.zip.ZipOutputStream;

public class ArchiveTask extends OpsTask<String, String[]> {
    public ArchiveTask(TestStatus status, TestList tests) {
        super(status, tests);
    }

    @Override
    protected String[] doInBackground(String... paths) {
        super.doInBackground(paths);
        String[] zippedPaths = new String[paths.length];
        int total = paths.length;
        for(int i = 0; i < total; i ++) {
            zippedPaths[i] = paths[i] + ".gz";
            zip(paths[i], zippedPaths[i]);
            publishProgress(i);
        }
        return zippedPaths;
    }

    public static boolean zip(String sourcePath, String zipPath) {
        Log.i("ZIP", "Zipping: " + sourcePath);
        boolean zipped = false;
        try {
            ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipPath));
            sourcePath = sourcePath.replaceAll("/$", "");
            String root = sourcePath.substring(0, sourcePath.lastIndexOf("/"));
            zipped = ZipUtil.zipHelper(sourcePath, root, zos);
            zos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return zipped;
    }
}
