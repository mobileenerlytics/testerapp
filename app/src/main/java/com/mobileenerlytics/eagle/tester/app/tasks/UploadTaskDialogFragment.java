package com.mobileenerlytics.eagle.tester.app.tasks;

import android.app.AlertDialog;
import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;

public class UploadTaskDialogFragment extends DialogFragment {
    private String message = "Problem occurred while uploading.";

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public @NonNull
    Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Upload Failed");
        builder.setMessage(message);
        return builder.create();
    }
}
