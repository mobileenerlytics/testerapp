package com.mobileenerlytics.eagle.tester.app.controllers;

import android.os.Bundle;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;

import com.mobileenerlytics.eagle.tester.app.MessageService;
import com.mobileenerlytics.eagle.tester.app.models.TestStatus;
import com.mobileenerlytics.eagle.tester.app.prefs.PrefUtils;
import com.mobileenerlytics.eagle.tester.app.util.MyServiceConnection;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * MessageHelper serves as the apps connection to the service connection. It is in charge of starting and stopping
 * tests with the appropriate settings.
 */
@Singleton
public class MessageHelper {
    private static final String TAG = "MessageHelper";
    // These values must match with EagleTester
    public static final int MSG_STOP = 0;
    public static final int MSG_START = 1;
    public static final int MSG_STOPPED_EPROF = 2;
    public static final int MSG_STARTED_EPROF = 3;
    public static final int MSG_PREPARE_DEVICE = 4;

    static final String LOGNAME = "tester_logname";

    // todo: should this be some other number?
    public static final int ARG_FAILED = 1;

    private TestStatus testStatus;
    private MyServiceConnection myServiceConnection;

    @Inject
    public MessageHelper(TestStatus status, MyServiceConnection myServiceConnection) {
        this.myServiceConnection = myServiceConnection;
        testStatus = status;
    }

    /**
     * Function to send message to serviceConnection to start logging.
     *
     * @param bitmap - blacklist of settings for the test
     * @param pkgName - packageName of AUT
     * @param testName - name of current test
     * @param commitHash - commitHash: AUT-<AUTbuildNumber>
     * @param keepAwake - flag to keep phone awake during duration of test
     */
    public void startLogging(int bitmap, String pkgName, String testName, String commitHash, boolean keepAwake) {
        if (myServiceConnection.getMessenger() == null) {
            Log.w(TAG, "MyServiceConnection is not set. Cannot begin logging.");
            return;
        } else if (testStatus.isStopping()) {
            Log.w(TAG, "Previous test in process of stopping. Wait until previous test has " +
                    "fully stopped before starting a new one.");
            return;
        } else if (!testStatus.isReady()) {
            Log.w(TAG, "Test already in progress. Stop current test before starting a new one.");
            return;
        }

        Message msg = Message.obtain(null, MSG_START, 0, 0);
        Bundle bundle = new Bundle();
        bundle.putString(MessageService.LOGNAME, "app");
        bundle.putString(PrefUtils.PACKAGE_KEY, pkgName);
        bundle.putString(PrefUtils.COMMIT_KEY, commitHash);
        bundle.putString(LOGNAME, testName);
        bundle.putBoolean(PrefUtils.AWAKE_KEY, keepAwake);
        msg.setData(bundle);
        msg.arg1 = bitmap; //blacklist
        msg.arg2 = (int) testStatus.selectedDuration.get()*1000;
        try {
            myServiceConnection.getMessenger().send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /** Function to send message to serviceConnection to stop current test */
    public void stopLogging() {
        if (myServiceConnection.getMessenger() == null) {
            Log.w(TAG, "MyServiceConnection is not set. Cannot stop logging.");
            return;
        } else if (testStatus.isStarting()) {
            Log.w(TAG, "Test has not yet fully started. Wait until loggers have completely started before stopping test.");
            return;
        } else if (!testStatus.isLogging()) {
            Log.w(TAG, "No test currently in progress. Cannot stop logging.");
            return;
        }
        try {
            Message msg = Message.obtain(null, MessageHelper.MSG_STOP, 0, 0);
            myServiceConnection.getMessenger().send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
