package com.mobileenerlytics.eagle.tester.app.controllers;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.annotation.VisibleForTesting;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.inject.Inject;
import javax.net.ssl.HttpsURLConnection;

public class AuthTask extends AsyncTask<Void, Void, Integer> {
    public static final String AUTH_KEY = "pref_auth";

    private final Context mContext;
    SharedPreferences sharedPreferences;

    @Inject
    public AuthTask(Context context, SharedPreferences sharedPreferences) {
        mContext = context;
        this.sharedPreferences = sharedPreferences;
    }

    @VisibleForTesting
    int authConnection(HttpsURLConnection httpURLConnection, String basicAuth) throws IOException {
        httpURLConnection.setRequestProperty("Authorization", "Basic " + basicAuth);
        httpURLConnection.connect();
        int code = httpURLConnection.getResponseCode();
        Log.d("HTTP", "Auth response: " + String.valueOf(code) + httpURLConnection.getResponseMessage());
        return code;
    }

    @Override
    protected Integer doInBackground(Void... v) {
        HttpsURLConnection httpURLConnection = null;
        try {
            httpURLConnection = (HttpsURLConnection) new URL("https://tester.mobileenerlytics.com/api/auth/").openConnection();
            String[] auth = sharedPreferences.getString(AUTH_KEY, "").split(":");
            if(auth.length != 2)
                return 0;
            String username = auth[0];
            String password = auth[1];
            byte[] encoded = (username + ":" + password).getBytes(StandardCharsets.UTF_8);
            String basicAuth = Base64.encodeToString(encoded, Base64.NO_WRAP);
            int code = authConnection(httpURLConnection, basicAuth);
            return code;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            httpURLConnection.disconnect();
            Log.d("HTTP", "disconnected");
        }
        return 0;
    }

    @VisibleForTesting
    @Override
    protected void onPostExecute(Integer code) {
        if(code != 200)
            Toast.makeText(mContext, "Authentication failed. Update username or password, or check connection.", Toast.LENGTH_LONG).show();
        else
            Toast.makeText(mContext, "Authentication successful", Toast.LENGTH_SHORT).show();
    }
}
