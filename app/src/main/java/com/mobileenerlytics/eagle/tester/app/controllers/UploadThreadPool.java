package com.mobileenerlytics.eagle.tester.app.controllers;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.Log;

import com.mobileenerlytics.eagle.tester.app.models.TestInfo;
import com.mobileenerlytics.eagle.tester.app.models.TestList;
import com.mobileenerlytics.eagle.tester.app.models.TestStatus;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;
import okio.BufferedSink;
import okio.ForwardingSink;
import okio.Okio;
import okio.Sink;

import static com.mobileenerlytics.eagle.tester.app.controllers.AuthTask.AUTH_KEY;
import static com.mobileenerlytics.eagle.tester.app.models.TestStatus.UPLOAD_ERROR.SUCCESS;
import static com.mobileenerlytics.eagle.tester.app.models.TestStatus.UPLOAD_ERROR.ZIP_FAIL;
import static com.mobileenerlytics.eagle.tester.app.prefs.SettingsFragment.URL_KEY;
import static com.mobileenerlytics.eagle.tester.app.tasks.DeleteTask.delete;

/**
 * When WorkManager runs UploadWorker it kicks off an UploadThreadPool. Sets TestStatus to PROCESSING and starts an UploadThread for
 * every TestInfo staged to Upload in TestList. UploadThread will create an OkHttp Request and process the response.
 *
 * @author Chase Porter
 * @since 2019-04-08
 */
@Singleton
public class UploadThreadPool {
    private final String TAG = UploadThreadPool.class.getSimpleName();
    public ExecutorService pool;
    private TestStatus testStatus;
    private TestList testList;
    private OkHttpClient httpClient;
    private String auth, url;
    private SharedPreferences sharedPreferences;

    @Inject
    UploadThreadPool(TestStatus testStatus, TestList testList, SharedPreferences sharedPreferences, OkHttpClient okHttpClient) {
        this.testStatus = testStatus;
        this.testList = testList;
        this.httpClient = okHttpClient;
        this.sharedPreferences = sharedPreferences;
    }

    public boolean run() {
        auth = sharedPreferences.getString(AUTH_KEY, ":");
        String baseUrl = sharedPreferences.getString(URL_KEY, "https://tester.mobileenerlytics.com");
        url = String.format("%s/api/upload/version_energy", baseUrl.replaceAll("/$", ""));
        if (auth == null) {
            Log.e(TAG, "Authentication needs to be set before attempting to upload any tests.");
            return false;
        }
        if (!testStatus.setProcessing()) {
            Log.e(TAG, "App is currently busy, cannot begin test uploads.");
            return false;
        }
        pool = Executors.newFixedThreadPool(3);
        for (TestInfo testInfo : testList.getProcessing()) {
            synchronized (testInfo) {
                if (!testInfo.queueTest())
                    continue;
                UploadThread uploadThread = new UploadThread(testInfo);
                testInfo.uploadFuture = pool.submit(uploadThread);
            }
        }
        pool.shutdown();
        boolean terminated;
        try {
            terminated = pool.awaitTermination(1, TimeUnit.HOURS);
        } catch (InterruptedException e) {
            terminated = false;
        }
        if (!terminated) {
            testList.reStageTests();
        }
        Log.i(TAG, "ThreadPool execution complete. Setting State to Ready.");
        testStatus.setReady();
        return terminated;
    }

    /** Builds an OkHttpRequest from UploadRequestBody and gets the Response from OkHttpClient.
     * Can't build a request outside this function and pass it instead of UploadRequestBody because
     * needed a way to unit Test that upload wrote the uploadProgress to UploadRequestBody. Can do that
     * this way by mocking this function and writing a value to uploadRequestBody directly in the mock.
     *
     * @param client - OkHttpClient posting Request to
     * @param uploadRequestBody - RequestBody to put in our Request to the client.
     * @return response code: e.g. 200, 401, etc.
     */
    @VisibleForTesting
    int getHttpResponse(OkHttpClient client, UploadRequestBody uploadRequestBody) {
        int flag = android.util.Base64.NO_WRAP;
        byte[] authString = auth.getBytes(StandardCharsets.UTF_8);
        String encoding = android.util.Base64.encodeToString(authString, flag);
        Request request = new Request.Builder()
            .url(url)
            .header("Authorization", "Basic " + encoding)
            .post(uploadRequestBody)
            .build();
        Log.d(TAG, request.toString());
        int responseCode = -1;
        try (Response response = client.newCall(request).execute()) {
            responseCode = response.code();
            Log.d(TAG, response.message());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return responseCode;
    }

    @VisibleForTesting
    int upload(File zippedFile, TestInfo testInfo) {
        if (!testInfo.prepareForUpload())
            return -1;
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("file", zippedFile.getName(), RequestBody.create(MediaType.parse("application/zip"), zippedFile))
                .addFormDataPart("pkg", testInfo.pkgName.get())
                .addFormDataPart("project_name", "default")
                .addFormDataPart("author_name", "manual-author-name")
                .addFormDataPart("author_email", "manual-test-email")
                .addFormDataPart("branch", "manual-test-branch")
                .addFormDataPart("commit", testInfo.commitHash.get())
                .addFormDataPart("current_version", "manual-test-version")
                .build();
        UploadRequestBody uploadRequestBody = new UploadRequestBody(requestBody, testInfo);
        return getHttpResponse(httpClient, uploadRequestBody);
    }

    /** Thread responsible for uploading a single TestInfo */
    public class UploadThread implements Runnable {
        TestInfo testInfo;

        UploadThread(TestInfo testInfo) {
            this.testInfo = testInfo;
        }

        @Override
        public void run() {
            if (isInterrupted())
                return;
            File testFile, zippedFile;
            try {
                testFile = testInfo.getTestDir();
                zippedFile = testInfo.zipTest();

            } catch (IOException ioe) {
                ioe.printStackTrace();
                testInfo.unStageTest();
                testStatus.uploadFailed.set(ZIP_FAIL);
                return;
            }
            if(isInterrupted()) {
                delete(zippedFile);
                return;
            }
            int uploadCode = upload(zippedFile, testInfo);
            delete(zippedFile);
            if(isInterrupted())
                return;
            if (uploadCode == SUCCESS) {
                delete(testFile);
                testList.remove(testInfo);
            } else {
                testInfo.unStageTest();
                testStatus.uploadFailed.set(uploadCode);
            }
        }

        boolean isInterrupted() {
            try {
                Thread.sleep(1);
                return false;
            } catch (InterruptedException e) {
                return true;
            }
        }

    }

    /**
     * Wrapper Class for OkHttp RequestBody that overwrites the writeTo method so that we have access to
     * the progress of the OkHttp Request and can display the progress of an upload to the user.
     */
    class UploadRequestBody extends RequestBody {
            RequestBody delegate;
            TestInfo testInfo;
            CountingOutputStream outputStream;

            UploadRequestBody(RequestBody requestBody, TestInfo testInfo) {
                this.delegate = requestBody;
                this.testInfo = testInfo;
            }

            @Override
            public MediaType contentType() {
                return delegate.contentType();
            }

            @Override
            public void writeTo(@NonNull  BufferedSink sink) throws IOException {
                BufferedSink bufferedSink;
                outputStream = new CountingOutputStream(sink);
                bufferedSink = Okio.buffer(outputStream);
                delegate.writeTo(bufferedSink);
                bufferedSink.flush();
            }

            class CountingOutputStream extends ForwardingSink {
                long bytesWritten = 0;

                CountingOutputStream(Sink delegate) {
                    super(delegate);
                }

                @Override
                public void write(@NonNull Buffer source, long byteCount) throws IOException {
                    super.write(source, byteCount);
                    bytesWritten += byteCount;
                    testInfo.uploadProgressMB.set(Math.floor(bytesWritten / 10000.) / 100);
                }
            }
        }
}

