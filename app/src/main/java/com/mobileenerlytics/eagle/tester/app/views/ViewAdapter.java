package com.mobileenerlytics.eagle.tester.app.views;

import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mobileenerlytics.eagle.tester.app.BR;
import com.mobileenerlytics.eagle.tester.app.R;
import com.mobileenerlytics.eagle.tester.app.models.TestInfo;
import com.mobileenerlytics.eagle.tester.app.models.TestList;
import com.mobileenerlytics.eagle.tester.app.models.TestStatus;

public class ViewAdapter extends RecyclerView.Adapter<ViewAdapter.TestInfoViewHolder> {
    private final String TAG = ViewAdapter.class.getSimpleName();
    private TestList testList;
    private ObservableArrayList<TestInfo> testInfos;
    private LayoutInflater layoutInflater;
    private static final ViewAdapter mInstance = new ViewAdapter();
    private TestStatus testStatus;

    private static ViewAdapter getInstance(TestList testList, TestStatus testStatus) {
        mInstance.testList = testList;
        mInstance.testStatus = testStatus;
        mInstance.testInfos = testList.testInfos;
        return mInstance;
    }

    static class TestInfoViewHolder extends RecyclerView.ViewHolder {
        ViewDataBinding binding;
        TestInfo testInfo;

        TestInfoViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void setTestInfo(TestInfo testInfo) {
            this.testInfo = testInfo;
            binding.setVariable(BR.data, testInfo);
            binding.setVariable(BR.status, mInstance.testStatus);
            binding.setVariable(BR.testList, mInstance.testList);
            binding.executePendingBindings();
        }
    }

    @NonNull
    @Override
    public TestInfoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(layoutInflater, viewType, parent, false);
        return new TestInfoViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(TestInfoViewHolder holder, final int position) {
        holder.setTestInfo(testInfos.get(position));
    }

    @Override
    public int getItemViewType(int i) {
        if (testInfos.get(i).isUnchecked() || testInfos.get(i).isChecked()) {
            return R.layout.listview_item;
        } else {
            return R.layout.listview_uploaditem;
        }
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemCount() {
        return testInfos.size();
    }

    @BindingAdapter({"entries", "testStatus"})
    public static void setEntries(RecyclerView testListView, TestList testList, TestStatus testStatus) {
        testListView.getRecycledViewPool().setMaxRecycledViews(R.layout.listview_item, 0);
        testListView.getRecycledViewPool().setMaxRecycledViews(R.layout.listview_uploaditem, 0);
        testListView.setAdapter(getInstance(testList, testStatus));
    }

    public static void updateTest(TestInfo testInfo) {
        mInstance.notifyItemChanged(mInstance.testInfos.indexOf(testInfo));
    }
}
