package com.mobileenerlytics.eagle.tester.app.prefs;

import android.content.SharedPreferences;

import com.mobileenerlytics.eprof.logger.EprofLoggerMain;

public class PrefUtils {
    private static final String CURRENT_SENSOR_KEY = "pref_sensor";
    private static final String LAUNCH_KEY = "pref_launch";
    public static final String AWAKE_KEY = "pref_awake";
    public static final String SCREEN_KEY = "pref_screen";
    public static final String STRACE_KEY = "pref_strace";
    public static final String BUGREPORT_KEY = "pref_bugreport";
    public static final String NETWORK_KEY = "pref_network";
    public static final String PACKAGE_KEY = "packageName";
    public static final String TEST_KEY = "tester_logname";
    public static final String COMMIT_KEY = "commitHash";
    public static final String DURATION_MS_KEY = "durationInMs";

    public static int getBitmap(SharedPreferences sharedPreferences) {
        boolean recordScreen = sharedPreferences.getBoolean(SCREEN_KEY, false);
        boolean logStrace = sharedPreferences.getBoolean(STRACE_KEY, false);
        boolean logBugreport = sharedPreferences.getBoolean(BUGREPORT_KEY, false);
        boolean logNetwork = sharedPreferences.getBoolean(NETWORK_KEY, false);
        boolean currentSensor = sharedPreferences.getBoolean(CURRENT_SENSOR_KEY, false);
        int bitmap = 0; // blacklist
        if (!recordScreen) bitmap |= EprofLoggerMain.LoggedComponent.SCREEN_VIDEO.bitmap;
        if (!logStrace) bitmap |= EprofLoggerMain.LoggedComponent.STRACE.bitmap;
        if (!logBugreport) bitmap |= EprofLoggerMain.LoggedComponent.HISTORIAN.bitmap;
        if (!logNetwork) bitmap |= EprofLoggerMain.LoggedComponent.NET.bitmap;
        if (!currentSensor) bitmap |= EprofLoggerMain.LoggedComponent.CURRENT_SENSOR.bitmap;
        return bitmap;
    }

    public static boolean shouldLaunch(SharedPreferences sharedPreferences) {
        return (sharedPreferences.getBoolean(LAUNCH_KEY, false));
    }

    public static boolean shouldKeepAwake(SharedPreferences sharedPreferences) {
        return (sharedPreferences.getBoolean(AWAKE_KEY, false));
    }
}
