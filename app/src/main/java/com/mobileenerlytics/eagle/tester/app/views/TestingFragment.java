package com.mobileenerlytics.eagle.tester.app.views;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.mobileenerlytics.eagle.tester.app.MainApplication;
import com.mobileenerlytics.eagle.tester.app.R;
import com.mobileenerlytics.eagle.tester.app.controllers.MessageHelper;
import com.mobileenerlytics.eagle.tester.app.controllers.TestNotifier;
import com.mobileenerlytics.eagle.tester.app.dagger.AppComponent;
import com.mobileenerlytics.eagle.tester.app.databinding.TestingFragmentBinding;
import com.mobileenerlytics.eagle.tester.app.models.AppInfo;
import com.mobileenerlytics.eagle.tester.app.models.AppList;
import com.mobileenerlytics.eagle.tester.app.models.TestList;
import com.mobileenerlytics.eagle.tester.app.models.TestStatus;
import com.mobileenerlytics.eagle.tester.app.models.TestStatus.STATUS;
import com.mobileenerlytics.eagle.tester.app.prefs.PrefUtils;
import com.mobileenerlytics.eagle.tester.app.tasks.UploadTaskDialogFragment;

import javax.inject.Inject;

import static com.mobileenerlytics.eagle.tester.app.models.TestStatus.STATUS.READY;
import static com.mobileenerlytics.eagle.tester.app.models.TestStatus.UPLOAD_ERROR.AUTH_FAIL;
import static com.mobileenerlytics.eagle.tester.app.models.TestStatus.UPLOAD_ERROR.NETWORK_FAIL;
import static com.mobileenerlytics.eagle.tester.app.models.TestStatus.UPLOAD_ERROR.ZIP_FAIL;
import static com.mobileenerlytics.eagle.tester.app.models.TestStatus.UPLOAD_ERROR.SUCCESS;

public class TestingFragment extends Fragment {
    private Button mainButton;
    private Spinner durationSpinner;
    private BottomSheetBehavior bottomSheetBehavior;
    private ImageButton sheetButton;
    private Observable.OnPropertyChangedCallback errorMessageCallback;
    private AppComponent appComponent;
    private Chronometer chronometer;
    @Inject
    public SharedPreferences sharedPreferences;
    @Inject
    public TestStatus testStatus;
    @Inject
    public TestList testList;
    @Inject
    public AppList appList;
    @Inject
    public MessageHelper messageHelper;
    @Inject
    public TestNotifier testNotifier;
    private final String EAGLE_TESTER = "com.mobileenerlytics.eagle.tester.app";

    private final String SELECTED_APP = "selected_app";
    private final String SELECTED_DURATION = "selected_duration";
    private final String TEST_NAME = "test_name";

    private final String ERROR_MESSAGE = "An unexpected problem occurred while uploading. Failed log(s) are saved in the files tab. Please try to upload again later or contact us at support@mobileenerlytics.com if the problem persists.";
    private final String NETWORK_ERROR = "A Network Error occurred, upload aborted. Please check you network connection. Failed log(s) are saved in the files tab.";
    private final String AUTH_ERROR = "Authentication Error occurred, upload aborted. Please check your username and password. Failed log(s) are saved in the files tab.";
    private final String ZIP_ERROR = "Test(s) failed to zip. Check file format. Failed log(s) are saved in the files tab.";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        appComponent = ((MainApplication)getActivity().getApplicationContext()).getAppComponent();
        appComponent.inject(this);
        errorMessageCallback = new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                if (!testStatus.isReady() || testStatus.uploadFailed.get() == SUCCESS)
                    return;
                int exitCode = testStatus.uploadFailed.get();
                UploadTaskDialogFragment uploadTaskDialogFragment = new UploadTaskDialogFragment();
                switch (exitCode) {
                    case AUTH_FAIL:
                        uploadTaskDialogFragment.setMessage(AUTH_ERROR);
                        break;
                    case NETWORK_FAIL:
                        uploadTaskDialogFragment.setMessage(NETWORK_ERROR);
                        break;
                    case ZIP_FAIL:
                        uploadTaskDialogFragment.setMessage(ZIP_ERROR);
                        break;
                    default:
                        uploadTaskDialogFragment.setMessage(ERROR_MESSAGE);
                        break;
                }
                uploadTaskDialogFragment.setTargetFragment(getParentFragment(), 0);
                uploadTaskDialogFragment.show(getFragmentManager(), "uploadDialog");
                testStatus.uploadFailed.set(SUCCESS);
            }
        };
        testStatus.status.addOnPropertyChangedCallback(errorMessageCallback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        TestingFragmentBinding binding = DataBindingUtil.inflate(inflater, R.layout.testing_fragment,
                container, false);
        View view = binding.getRoot();
        mainButton = view.findViewById(R.id.mainButton);
        durationSpinner = view.findViewById(R.id.durationSpinner);
        binding.setStatus(testStatus);
        binding.setTestList(testList);
        binding.setAppList(appList);
        RecyclerView testListView = view.findViewById(R.id.lView);
        testListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        bottomSheetBehavior = BottomSheetBehavior.from(view.findViewById(R.id.scrollView));
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        sheetButton = view.findViewById(R.id.sheetButton);
        chronometer = view.findViewById(R.id.loggingTimer);
        setOnClickListeners();
        populateDurationList(sharedPreferences.getInt(SELECTED_DURATION, 0));
        testStatus.testName.set(sharedPreferences.getString(TEST_NAME, ""));
        appList.setSelectedAppByPackage(sharedPreferences.getString(SELECTED_APP, ""));
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        testStatus.status.removeOnPropertyChangedCallback(errorMessageCallback);
    }

    @Override
    public void onResume() {
        super.onResume();
        // Restart chronometer if required
        UiAdapter.setBase(chronometer, testStatus);
    }

    @Override
    public void onPause() {
        super.onPause();
        // Stop chronometer so we don't incur GPU energy even
        // when app is not in foreground
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(SELECTED_DURATION, durationSpinner.getSelectedItemPosition());
        editor.putString(SELECTED_APP, appList.selectedApp.get().pkgName.get());
        editor.putString(TEST_NAME, testStatus.testName.get());
        editor.apply();
        chronometer.stop();
    }

    private void populateDurationList(int index) {
        ArrayAdapter<CharSequence> adapter =
                ArrayAdapter.createFromResource(getActivity(), R.array.durations_array,
                        android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        durationSpinner.setAdapter(adapter);
        durationSpinner.setSelection(index);
    }

    private void setOnClickListeners() {
        durationSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String testDuration = durationSpinner.getSelectedItem().toString();
                if (testDuration.equals("Untimed")) {
                    testStatus.selectedDuration.set(-1);
                } else {
                    int sleepMinutes = Integer.valueOf(testDuration.split("\\s+")[0]);
                    testStatus.selectedDuration.set(60 * sleepMinutes);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (testStatus.getState() == READY) {
                    AppInfo selectedApp = appList.selectedApp.get();
                    String appVersion = selectedApp.appName.get() + "-" + selectedApp.versionName.get();
                    String pkgName = selectedApp.pkgName.get();
                    String testName = testStatus.testName.get() == "" ? "manual-test" : testStatus.testName.get();
                    if (PrefUtils.shouldLaunch(sharedPreferences) && !pkgName.equals(EAGLE_TESTER)) {
                        Intent launchIntent = getActivity().getPackageManager().getLaunchIntentForPackage(pkgName);
                        startActivity(launchIntent);
                    }
                    messageHelper.startLogging(PrefUtils.getBitmap(sharedPreferences),
                            pkgName, testName, appVersion, PrefUtils.shouldKeepAwake(sharedPreferences));
                } else if (testStatus.getState() == STATUS.LOGGING) {
                    messageHelper.stopLogging();
                } else {
                    Log.w("MAIN BUTTON", "Main button clicked in state: " + testStatus.getState());
                }
            }
        });

        sheetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                } else {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }
        });
    }
}
