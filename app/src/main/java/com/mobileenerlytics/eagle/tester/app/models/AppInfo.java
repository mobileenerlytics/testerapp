package com.mobileenerlytics.eagle.tester.app.models;

import android.content.pm.PackageManager;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableField;
import android.graphics.drawable.Drawable;

/**
 * AppInfo holds relevant information pulled from PackageManager needed to test a specific app identified
 * with its packageName
 *
 * Holds the appName, the buildNumber, and the icon for the app.
 *
 * @author Chase Porter
 * @since 2019-04-08
 */
public class AppInfo extends BaseObservable implements Comparable<AppInfo> {

    public final ObservableField<String> appName = new ObservableField<>();
    public final ObservableField<String> versionName = new ObservableField<>();
    public final ObservableField<Drawable> appIcon = new ObservableField<>();
    public final ObservablePkg pkgName = new ObservablePkg();

    public class ObservablePkg extends ObservableField<String> {
        @Override
        public void set(String value) {
            throw new UnsupportedOperationException();
        }
        public void set(String pkgName, PackageManager packageManager) {
            if( get() == null || ! get().equals(pkgName)) {
                super.set(pkgName);
                try {
                    Drawable icon = packageManager.getApplicationIcon(pkgName);
                    appIcon.set(icon);
                    String app = (String) packageManager.getApplicationLabel(
                            packageManager.getApplicationInfo(pkgName, PackageManager.GET_META_DATA));
                    String version = packageManager.getPackageInfo(pkgName, 0).versionName.split(" ")[0];
                    appName.set(app);
                    versionName.set(version);
                } catch (PackageManager.NameNotFoundException e) {
                    appIcon.set(packageManager.getDefaultActivityIcon());
                    appName.set(pkgName);
                }

            }
        }
        public void set(String value, String mAppName, Drawable mAppIcon) {
            super.set(value);
            appName.set(mAppName);
            appIcon.set(mAppIcon);
        }
    }

    public AppInfo(String mPkgName, PackageManager packageManager) {
        pkgName.set(mPkgName, packageManager);
    }

    @Override
    public int compareTo(AppInfo appInfo) {
        return this.appName.get().compareTo(appInfo.appName.get());
    }

    @Bindable
    public String getAppVersionName() { return appName.get() + " v" + versionName.get(); }

    @Bindable
    public Drawable getAppIcon() { return appIcon.get(); }
}
