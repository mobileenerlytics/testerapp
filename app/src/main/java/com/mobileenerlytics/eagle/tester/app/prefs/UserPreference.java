package com.mobileenerlytics.eagle.tester.app.prefs;

import android.content.Context;
import android.support.v7.preference.DialogPreference;
import android.support.v7.preference.PreferenceManager;
import android.util.AttributeSet;

public class UserPreference extends DialogPreference {
    public UserPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public UserPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public UserPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UserPreference(Context context) {
        super(context);
    }

    public String getUserName() {
        String auth = PreferenceManager
                .getDefaultSharedPreferences(getContext())
                .getString(getKey(), "user:password");
        if (auth.startsWith(":"))
            return "";
        return auth.split(":")[0];
    }

    public String getPassword() {
        String auth = PreferenceManager
                .getDefaultSharedPreferences(getContext())
                .getString(getKey(), "user:password");
        String[] authValues = auth.split(":");
        if (authValues.length == 2)
            return authValues[1];
        if (authValues.length == 1 && auth.startsWith(":"))
            return authValues[0];
        return "";
    }

    public void persist(String username, String password) {
        persistString(username + ":" + password);
    }
}
