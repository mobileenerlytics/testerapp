package com.mobileenerlytics.eagle.tester.app.tasks;

import android.util.Log;

import com.mobileenerlytics.eagle.tester.app.models.TestInfo;
import com.mobileenerlytics.eagle.tester.app.models.TestList;
import com.mobileenerlytics.eagle.tester.app.models.TestStatus;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

public class DeleteTask extends OpsTask<TestInfo, Void> {
    @Inject
    public DeleteTask(TestStatus status, TestList tests) {
        super(status, tests);
    }

    @Override
    protected Void doInBackground(TestInfo... testInfos) {
        super.doInBackground(testInfos);
        int total = testInfos.length;
        for(int i = 0; i < total; i ++) {
            Log.w("DELETING", "log: " + testInfos[i].logDir.get());
            try {
                delete(testInfos[i].getTestDir());
                testsToRemove.add(testInfos[i]);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
        return null;
    }

    // deletes files or shallow directory
    public static boolean delete(File file) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File f : files) {
                f.delete();
            }
        }
        return file.delete();
    }
}
