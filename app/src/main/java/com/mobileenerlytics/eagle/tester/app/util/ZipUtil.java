package com.mobileenerlytics.eagle.tester.app.util;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by abhilash on 26/04/18.
 */

public class ZipUtil {
    /**
     * sourcePath- "/storage/emulated/0/traces/eagle-5166679574251/build.prop"
     * root- "/storage/emulated/0/traces"
     */
    public static boolean zipHelper(String sourcePath, String root, ZipOutputStream zos) {
        int BUFFER = 2048;
        byte data[] = new byte[BUFFER];
        File source = new File(sourcePath);

        String zipPath = sourcePath.substring(root.length() + 1);
        ZipEntry entry = new ZipEntry(zipPath);
        if (!source.isDirectory()) {
            try {
                BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(sourcePath), BUFFER);
                zos.putNextEntry(entry);
                int count;
                while ((count = inputStream.read(data, 0, BUFFER)) != -1) {
                    zos.write(data, 0, count);
                }
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        } else {
            File files[] = source.listFiles();
            for (File file : files) {
                zipHelper(sourcePath + "/" + file.getName(), root, zos);
            }
        }
        return true;
    }
}
