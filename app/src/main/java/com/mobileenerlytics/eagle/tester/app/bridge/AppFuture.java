package com.mobileenerlytics.eagle.tester.app.bridge;

import android.support.annotation.NonNull;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class AppFuture implements Future<String> {
    private static final String TAG = "AppFuture";
    final Process proc;
    boolean mCancel = false;
    final long millis;

    public AppFuture(Process proc, long millis) {
        this.proc = proc;
        this.millis = millis;
    }

    @Override
    public boolean cancel(boolean b)  {
        // waitFor is blocking and hence risky. Use exitValue in a loop instead.
        int ctr = 0;
        while(++ctr <= 25) {    // try for 2.5 seconds
            Log.d(TAG, "Trying to destroy process. Attempt " + ctr);
            proc.destroy();
            try {
                proc.exitValue();
                Log.d(TAG, "Destroyed process in attempt " + ctr);
                break;
            } catch (IllegalThreadStateException e) {
                // Means the process is still running
                // https://developer.android.com/reference/java/lang/Process.html#exitValue()
            }
            try {
                /**
                 * This sleep timer has the following tradeoff-
                 *
                 * This is the amount of time we'll waste waiting for short commands to finish.
                 * This can be critical. For example, this timer adds directly to the user wait
                 * time after a user clicks on starts profiling and is waiting for profiling to
                 * begin.
                 *
                 * If the timer is however too small, we'll wake up over and over causing higher
                 * overhead.
                 */
                Thread.sleep(100);
            } catch (InterruptedException e) {
                /**
                 *  Warning: Do not remove this interrupt or else it will clear the interrupt status
                 *  leading to problems with status checking (e.g in PsLogger).
                 */
                Thread.currentThread().interrupt();
            }
        }
        closeStreams();
        mCancel = true;
        return true;
    }

    @Override
    public boolean isCancelled() {
        return mCancel;
    }

    @Override
    public boolean isDone() {
        try {
            proc.exitValue();
            return true;
        } catch (IllegalThreadStateException e) {
            return false;
        }
    }

    @Override
    public String get() throws InterruptedException, ExecutionException {
        long startMillis = System.currentTimeMillis();
        StringBuilder stringBuilder = new StringBuilder();
        final InputStream stream = proc.getInputStream();
        final int BUFF_LEN = 1024;
        final byte[] buffer = new byte[BUFF_LEN];
        ExecutorService executor = Executors.newFixedThreadPool(1);
        try {
            while (true) {
                Callable<Integer> readTask = new Callable<Integer>() {
                    @Override
                    public Integer call() throws Exception {
                        return stream.read(buffer);
                    }
                };
                Future<Integer> future = executor.submit(readTask);
                int read = future.get(millis, TimeUnit.MILLISECONDS);
                if (read < 0) {
                    Log.d(TAG, "Done reading the future.");
                    break;
                }
                stringBuilder.append(new String(buffer, 0, read));

                /*try {
                    proc.exitValue();
                    // Means proc has ended, let's break out.
                    break;
                } catch(IllegalThreadStateException e) {
                    // Means proc is still running, ignore
                }*/

                long currMillis = System.currentTimeMillis();
                long elapsed = currMillis - startMillis;
                if(elapsed > millis) {
                    // we timed out
                    Log.w(TAG, "Process timed out! Will destroy.");
                    break;
                }
            }
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        cancel(true);
        executor.shutdown();
        return stringBuilder.toString();
    }

    @Override
    public String get(long l, @NonNull TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        throw new UnsupportedOperationException("AppFuture doesn't support timeout");
    }

    private void closeStreams() {
        try {
            Log.d("Proc Streams", "Closing streams");
            proc.getInputStream().close();
            proc.getErrorStream().close();
            proc.getOutputStream().flush();
            proc.getOutputStream().close();
        } catch (IOException e) {
            Log.e("Proc Streams", e.toString());
        }
    }
}
