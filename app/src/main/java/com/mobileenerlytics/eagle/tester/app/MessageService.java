package com.mobileenerlytics.eagle.tester.app;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.Toast;

import com.mobileenerlytics.eagle.tester.app.bridge.AppDevice;
import com.mobileenerlytics.eagle.tester.app.loggers.CurrentSensorLogger;
import com.mobileenerlytics.eagle.tester.app.models.TestList;
import com.mobileenerlytics.eagle.tester.app.models.TestStatus;
import com.mobileenerlytics.eagle.tester.app.prefs.PrefUtils;
import com.mobileenerlytics.eagle.tester.app.views.MainActivity;
import com.mobileenerlytics.eprof.logger.EprofLogger;
import com.mobileenerlytics.eprof.logger.EprofLoggerMain;
import com.mobileenerlytics.eprof.logger.EprofLoggerMain.LoggedComponent;
import com.mobileenerlytics.eprof.logger.IEprofChangeListener;
import com.mobileenerlytics.eprof.logger.bridge.IDeviceBridge;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

import javax.inject.Inject;

import static com.mobileenerlytics.eagle.tester.app.controllers.MessageHelper.ARG_FAILED;
import static com.mobileenerlytics.eagle.tester.app.controllers.MessageHelper.MSG_PREPARE_DEVICE;
import static com.mobileenerlytics.eagle.tester.app.controllers.MessageHelper.MSG_START;
import static com.mobileenerlytics.eagle.tester.app.controllers.MessageHelper.MSG_STARTED_EPROF;
import static com.mobileenerlytics.eagle.tester.app.controllers.MessageHelper.MSG_STOP;
import static com.mobileenerlytics.eagle.tester.app.controllers.MessageHelper.MSG_STOPPED_EPROF;
import static com.mobileenerlytics.eagle.tester.app.prefs.PrefUtils.AWAKE_KEY;

/** MessageService communicates with EprofLoggerMain to coordinate starting and stopping logging
 * of tests. Receives messages from MessageHelper to start testing and has methods EprofLogger calls
 * when testing has begun and stopped.
 *
 * Schedules stop messages to be be run after a time delay if a duration > -1 is received.
 */
public class MessageService extends Service implements IEprofChangeListener {
    private static final int UNIQUE_ID = 38673;
    private static final String TAG = "MessageService";
    private static EprofLoggerMain loggerMain;

    public static final String SUCCESS = "success";
    public static final String LOGDIR = "logdir";

    // build.props
    public static final String LOGNAME = "tester_logname";
    static final String VERSION_NAME = "tester_app_vname";
    static final String VERSION_CODE = "tester_app_vcode";
    static final String BRIGHTNESS = "brightness";
    private int brightnessMode = Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC;

    static String packageName = "";
    @Inject
    public TestStatus testStatus;
    @Inject
    public Context context;
    @Inject
    public TestList testList;

    private volatile  Messenger clientMessenger;

    private volatile boolean abort;

    private static PowerManager.WakeLock wakelock;

    /**
     * Function called from EprofLogger once logging has started. Send a message to MessageHelper
     * that logging has started and set the startTime of the test.
     */
    @Override
    public void startedEprof() {
        Message msg = Message.obtain(null, MSG_STARTED_EPROF);
        try {
            if (clientMessenger != null)
                clientMessenger.send(msg);
            else
                Log.w(TAG, "clientMessenger is not set. Skipping messaging.");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "Started eprof");
        testStatus.lastTestInfo.startRealTime.set(SystemClock.elapsedRealtime());
        testStatus.setLogging();
    }

    /**
     * Function called from EprofLogger on a successful stop of a test. In this case update tests
     * logDir with location of test folder on phone and add test to TestList to be displayed to user.
     *
     * If test was resetted or aborted then test needs to be deleted.
     *
     * Send a message to MessageHandler that EprofLogger has stopped and set the TestStatus to READY.
     * Function
     * @param success - test executed succesfully.
     * @param logDir - location of test directory on phone
     */
    @Override
    public void stoppedEprof(boolean success, String logDir) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(SUCCESS, success);
        if(abort) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, "Test aborted", Toast.LENGTH_SHORT).show();
                }
            });
            try {
                File f = new File(logDir);
                FileUtils.deleteDirectory(f);
                Log.w(TAG, "Successfully deleted aborted test's directory " + logDir);
            } catch (IOException e) {
                Log.w(TAG, "Failed to delete " + logDir + ". Tester might show energy for a " +
                        "failed test.");
                e.printStackTrace();
            }
        } else {
            bundle.putString(LOGDIR, logDir);
        }
        abort = false;
        if (success) {
            testStatus.lastTestInfo.logDir.set(logDir);
            testList.add(testStatus.lastTestInfo);
        }
        Message msg = Message.obtain(null, MSG_STOPPED_EPROF);
        msg.setData(bundle);
        try {
            if(clientMessenger != null)
                clientMessenger.send(msg);
            else
                Log.w(TAG, "clientMessenger is not set. Skipping messaging.");


        } catch (RemoteException e) {
            e.printStackTrace();
        }
        testStatus.setReady();
        stopForeground(true);
        if(wakelock.isHeld()) {
            Log.i(TAG, "Releasing wakelock");
            wakelock.release();
        }
    }

    @Override
    public void resettedEprof(String logDir) {
        stoppedEprof(false, logDir);
    }

    @Override
    public void processedTraces(boolean b) {
    }

    /**
     * Function called after a scheduled duration, after the stop button is clicked, or on a test failure.
     * Sets TestStatus to STOPPING
     * Sets the final duration of the test, aborts the test if necessary, and otherwise stops the test.
     *
     * @param msgFail - flag indicating the test logging failed.
     */
    public void stopLogging(boolean msgFail) {
        testStatus.setStopping();
        long durationInMs = SystemClock.elapsedRealtime() - testStatus.lastTestInfo.startRealTime.get();
        testStatus.lastTestInfo.setDuration(durationInMs);
        abort = abort | msgFail;
        if (abort)
            loggerMain.reset(null);
        else
            loggerMain.stop(null);

        // Reset screen brightness mode
        ContentResolver contentResolver = getContentResolver();
        Settings.System.putInt(contentResolver, Settings.System.SCREEN_BRIGHTNESS_MODE, brightnessMode);
    }

    /**
     * Sets the screen brightness adjustment mode to manual so it doesn't change during the test.
     * This ensures better repeatability and allows reliably reading the current screen brightness.
     *
     * @return current screen brightness
     */
    private int readBrightness() {
            ContentResolver contentResolver = getContentResolver();
            try {
                brightnessMode = Settings.System.getInt(contentResolver, Settings.System.SCREEN_BRIGHTNESS_MODE);
                Settings.System.putInt(
                        contentResolver,
                        Settings.System.SCREEN_BRIGHTNESS_MODE,
                        Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
                return Settings.System.getInt(
                        contentResolver,
                        Settings.System.SCREEN_BRIGHTNESS);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                Log.e(TAG, "Unable to read brightness. Assuming full brightness");
                return 255;
            }
        }


    static class LoggerHandler extends Handler {
        private final WeakReference<MessageService> mServiceReference;

        LoggerHandler(MessageService service, Looper looper) {
            super(looper);
            mServiceReference = new WeakReference<MessageService>(service);
        }

        /**
         * Handles incoming message from MessageHelper.
         *
         * On prepare_device: creates batteryManager and creates instance of EprofLogger
         *
         * On start: starts service in the foreground so it OS doesn't kill it, creates a directory for test logs,
         * schedules an automatic MSG_STOP to trigger after the passed duration if its > -1, and starts EProfLogger
         *
         * On stop: cancels all queued messages (if a test has been stopped early) and calls stopLogging
         *
         * @param msg - msg received from MessageHelper: MSG_PREPARE_DEVICE, MSG_STOP, MSG_START
         */
        @Override
        public void handleMessage(Message msg) {
            final MessageService service = mServiceReference.get();
            if(service == null) {
                Log.w(TAG, "No service live. Skipping msg " + msg);
                return;
            }
            switch (msg.what) {
                case MSG_PREPARE_DEVICE:
                    // This message might be received in case service restarts or is bound to again,
                    // don't recreate a new loggerMain if it already exists
                    if(loggerMain == null) {
                        IDeviceBridge device = new AppDevice(service);
                        loggerMain = new EprofLoggerMain(device);
                        final BatteryManager batteryManager = (BatteryManager)service.context.getSystemService(BATTERY_SERVICE);
                        LinkedList<EprofLogger> eprofLoggers = new LinkedList<EprofLogger>() {{
                           add(new CurrentSensorLogger(loggerMain, batteryManager));
                        }};
                        loggerMain.init(eprofLoggers, BuildConfig.DEBUG);
                        try {
                            loggerMain.prepareDevice();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        loggerMain.addEprofChangeListener(service);
                    }
                    break;
                // msg.arg1 = flag for test failure
                case MSG_STOP:
                    removeMessages(MSG_STOP);
                    service.stopLogging(msg.arg1 == ARG_FAILED);
                    break;
                // msg.data = bundle with testName, packageName, commitHash
                // msg.arg1 = blacklist of features
                // msg.arg2 = selectedDuration
                case MSG_START:
                    service.clientMessenger = msg.replyTo;
                    // Make the service foreground so it doesn't get killed
                    Intent notificationIntent = new Intent(service, MainActivity.class);
                    PendingIntent pendingIntent =
                        PendingIntent.getActivity(service, 0, notificationIntent, 0);
                    Notification.Builder notificationBuilder = new Notification.Builder(service);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        String channelId = service.createNotificationChannel(MessageService.class.getSimpleName(),
                                MainApplication.class.getSimpleName());
                        notificationBuilder.setChannelId(channelId);
                    }
                    Notification notification =
                        notificationBuilder
                        .setContentTitle(service.getText(R.string.notification_title))
                        .setContentText(service.getText(R.string.notification_message))
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentIntent(pendingIntent)
                        .build();
                    service.startForeground(UNIQUE_ID, notification);
                    service.testStatus.setStarting();
                    // Start logging
                    Bundle bundle = msg.getData();
                    final boolean keepAwake = bundle.getBoolean(AWAKE_KEY);
                    if(keepAwake) {
                        Log.i(TAG, "Acquiring wakelock");
                        wakelock.acquire();
                    }


                    final String logName = bundle.getString(LOGNAME);
                    packageName = bundle.getString(PrefUtils.PACKAGE_KEY);
                    final String commitHash = bundle.getString(PrefUtils.COMMIT_KEY);
                    service.testStatus.initializeTest(packageName, commitHash, logName);
                    loggerMain.setPackageName(packageName);
                    StringBuilder stringBuilder = new StringBuilder("" + System.currentTimeMillis());
                    final String revMillis = stringBuilder.reverse().toString();
                    final String extStorage = System.getenv("EXTERNAL_STORAGE");
                    final String logDir = String.format("%s/traces/eagle-%s/", extStorage, revMillis);
                    final File f = new File(logDir);
                    if(f.mkdirs()) {
                        HashSet<LoggedComponent> enabledComps = getEnabledComponents(msg.arg1);
                        service.loggerMain.start(logDir, false, null, enabledComps, new HashMap<String, String>() {{
                            put(LOGNAME, logName);
                            put(VERSION_NAME, BuildConfig.VERSION_NAME);
                            put(VERSION_CODE, "" + BuildConfig.VERSION_CODE);
                            put(PrefUtils.COMMIT_KEY, commitHash);
                            put(BRIGHTNESS, "" + service.readBrightness());
                        }}, null);
                    } else {
                        service.abort = true;
                    }
                    long durationInMs = msg.arg2;
                    if (durationInMs > 0) {
                        Message stopMsg = Message.obtain(null, MSG_STOP, 0, 0);
                        sendMessageDelayed(stopMsg, durationInMs);
                    }
                    break;
                default:
                    Log.w(TAG, "Ignoring unrecognized message. msg.what=" + msg.what);
                    super.handleMessage(msg);
            }
        }


        private HashSet<LoggedComponent> getEnabledComponents(int ignoredComponents) {
            HashSet<LoggedComponent> result = new HashSet<>();
            for (LoggedComponent comp : LoggedComponent.values()) {
                if ((comp.bitmap & ignoredComponents) == 0) {
                    result.add(comp);
                }
            }
            return result;
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(String channelId, String channelName) {
        NotificationChannel chan = new NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_NONE);
        NotificationManager service = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        service.createNotificationChannel(chan);
        return channelId;
    }

    @Override
    public IBinder onBind(Intent intent) {
        HandlerThread mHandlerThread = new HandlerThread("HandlerThread");
        mHandlerThread.start();
        Messenger mMessenger = new Messenger(new LoggerHandler(this, mHandlerThread.getLooper()));
        try {
            Message msg = Message.obtain(null, MSG_PREPARE_DEVICE);
            mMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return mMessenger.getBinder();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ((MainApplication) getApplication().getApplicationContext()).getAppComponent().inject(this);
        if(wakelock == null) {
            PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
            wakelock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "Eagle::Logger");
        }
    }

    @Override
    public void onDestroy() {
        if (testStatus.isLogging())
            stopLogging(true);
        super.onDestroy();
//        loggerMain.removeEprofChangeListener(this);
    }
}
