package com.mobileenerlytics.eagle.tester.app.views;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.IdRes;
import android.support.v4.app.ActivityCompat;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mobileenerlytics.eagle.tester.app.MainApplication;
import com.mobileenerlytics.eagle.tester.app.R;
import com.mobileenerlytics.eagle.tester.app.util.SetupVerification;

public class VerificationActivity extends Activity {

    private String TAG = "Verify";
    private final int REQUEST_WRITE_EXTERNAL_STORAGE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verification);
    }

    private void setCheck(@IdRes int ok, @IdRes int fail, boolean passed) {
        if(passed) {
            findViewById(ok).setVisibility(View.VISIBLE);
            findViewById(fail).setVisibility(View.INVISIBLE);
            findViewById(R.id.issueText).setVisibility(View.INVISIBLE);
            findViewById(R.id.fixIssue).setVisibility(View.INVISIBLE);
            findViewById(R.id.issueLink).setVisibility(View.INVISIBLE);
            findViewById(R.id.issueButton).setVisibility(View.INVISIBLE);
            findViewById(R.id.configButton).setVisibility(View.VISIBLE);
        } else {
            findViewById(ok).setVisibility(View.INVISIBLE);
            findViewById(fail).setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        verify();
    }

    private void verify() {
        boolean storage = SetupVerification.verifyStorage(this);
        setCheck(R.id.storageOK, R.id.storageFail, storage);

        boolean rooted = SetupVerification.verifyRooted();
        setCheck(R.id.rootedOK, R.id.rootedFail, rooted);

        boolean settings = SetupVerification.verifySettings(this);
        setCheck(R.id.settingsOK, R.id.settingsFail, settings);

        boolean ready = storage && settings && rooted;
        setCheck(R.id.readyOK, R.id.readyFail, ready);
        if (ready) {
            findViewById(R.id.readyOK).setVisibility(View.VISIBLE);
            Button button = findViewById(R.id.configButton);
            button.setVisibility(View.VISIBLE);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ((MainApplication)getApplication()).getAppComponent().getTestList().populateTracesList();
                    Intent intent = new Intent(VerificationActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            });
        } else {
            setupRemedy(storage, settings, rooted);
            Button button = findViewById(R.id.configButton);
            button.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            String permissions[],
            int[] grantResults) {
        verify();
    }

    private void setupRemedy(boolean storage, boolean settings, boolean rooted) {
        findViewById(R.id.fixIssue).setVisibility(View.VISIBLE);
        findViewById(R.id.issueText).setVisibility(View.VISIBLE);
        if (!storage)
            remedyStorage();
        else if(!settings)
            remedySettings();
        else if (!rooted) {
            remedyInstall();
        }
    }

    private void remedySettings() {
        ((TextView) findViewById(R.id.issueText)).setText("Allow setting screen brightness");
        Button button = findViewById(R.id.issueButton);
        button.setVisibility(View.VISIBLE);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                intent.setData(Uri.parse("package:" + getPackageName()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    private void remedyStorage() {
        ((TextView) findViewById(R.id.issueText)).setText("Grant storage permission");
        Button button = findViewById(R.id.issueButton);
        button.setVisibility(View.VISIBLE);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ActivityCompat.requestPermissions(VerificationActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_WRITE_EXTERNAL_STORAGE);
            }
        });
    }

    private void remedyInstall() {
        String linkText = "Visit the <a href='https://tester.mobileenerlytics.com/instructions.html'>Mobile Enerlytics↗</a> instructions page.";
        ((TextView) findViewById(R.id.issueText)).setText("Root phone");
        findViewById(R.id.issueLink).setVisibility(View.VISIBLE);
        TextView issueLink = findViewById(R.id.issueLink);
        issueLink.setText(Html.fromHtml(linkText));
        issueLink.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
