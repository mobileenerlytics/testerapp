package com.mobileenerlytics.eagle.tester.app.tasks;

import android.os.AsyncTask;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;

import com.mobileenerlytics.eagle.tester.app.models.TestInfo;
import com.mobileenerlytics.eagle.tester.app.models.TestList;
import com.mobileenerlytics.eagle.tester.app.models.TestStatus;

import java.util.ArrayList;

public abstract class OpsTask<P, R> extends AsyncTask<P, Integer, R> {
    protected final TestStatus mStatus;
    protected int mTotalWork;
    protected ArrayList<TestInfo> testsToRemove;
    protected TestList testList;

    OpsTask(@NonNull TestStatus status, @NonNull TestList tests) {
        mStatus = status;
        testList = tests;
        testsToRemove = new ArrayList<>();
    }

    @CallSuper
    @Override
    protected R doInBackground(P... t) {
        mTotalWork = t.length;
        return null;
    }

    @CallSuper
    @Override
    protected void onPreExecute() {
        mStatus.setProcessing();
    }

    @CallSuper
    @Override
    protected void onPostExecute(R r) {
        testList.removeAll(testsToRemove);
        mStatus.setReady();
    }
}
