package com.mobileenerlytics.eagle.tester.app.dagger;

import com.mobileenerlytics.eagle.tester.app.MessageService;
import com.mobileenerlytics.eagle.tester.app.controllers.AuthTask;
import com.mobileenerlytics.eagle.tester.app.controllers.MessageBroadcastReceiver;
import com.mobileenerlytics.eagle.tester.app.controllers.MessageHelper;
import com.mobileenerlytics.eagle.tester.app.controllers.TestNotifier;
import com.mobileenerlytics.eagle.tester.app.controllers.UploadWorker;
import com.mobileenerlytics.eagle.tester.app.models.AppList;
import com.mobileenerlytics.eagle.tester.app.models.TestList;
import com.mobileenerlytics.eagle.tester.app.models.TestStatus;
import com.mobileenerlytics.eagle.tester.app.prefs.UserPreferenceDialogFragmentCompat;
import com.mobileenerlytics.eagle.tester.app.util.MyServiceConnection;
import com.mobileenerlytics.eagle.tester.app.views.TestingFragment;

import javax.inject.Singleton;

import dagger.Component;

/** Dagger2 component for dependency injection. To inject a component A, it needs @Inject above its
 * constructor and it may need a method decleration A getA(); below.
 * Sometimes @Inject is enough, but the method decleration gives access to a function that can be used to
 * provide mocked dependencies to injected components: see injecting PackageManager in AppListTest
 *
 * The following models can be injected:
 *      {@link AppList}, {@link AuthTask}, {@link TestList}, {@link TestStatus}, {@link MyServiceConnection},
 *      {@link MessageHelper}, {@link TestNotifier}
 * The following other classes can also be injected:
 *      PackageManager, SharedPreferences, OkHttpClient, Context
 * They are all singletons with lifecycles tied to the lifecycle of MainApplication which holds
 * the appComponent
 *
 * There are two main ways to inject into a new class A.
 *    1) If all A's dependencies can be injected (either are own classes marked with @Inject or
 *       have a provider method in AppModule) then the preferred method is to inject via the constructor:
 *
 * @Inject
 * A(InjectableDependency b) {
 *     this.b = b:
 * }
 *
 *    2) If A has a dependency that cannot be injected (C) and one that can be injected (B) then
 *       a inject(A a) method must be defined below. Then if A has access to MainApplication in A do:
 * A {
 *   C c
 *   @Inject
 *   B b
 *
 *   onCreate() {
 *       MainApplication.getAppComponent().inject(this)
 *   }
 * }
 *
 * @author Chase Porter
 * @since 2019-04-08
 */
@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
    AppList getAppList();
    AuthTask getAuthTask();
    TestList getTestList();
    TestStatus getTestStatus();
    MyServiceConnection getMyServiceConnection();
    void inject(UserPreferenceDialogFragmentCompat userPreferenceDialogFragmentCompat);
    void inject(TestingFragment testingFragment);
    void inject(MessageService messageService);
    void inject(MessageBroadcastReceiver broadcastReceiver);
    void inject(UploadWorker uploadWorker);
}
