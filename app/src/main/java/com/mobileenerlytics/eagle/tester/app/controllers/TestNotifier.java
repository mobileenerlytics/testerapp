package com.mobileenerlytics.eagle.tester.app.controllers;

import android.content.Context;
import android.databinding.Observable;
import android.databinding.ObservableInt;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.mobileenerlytics.eagle.tester.app.models.TestStatus;

import static com.mobileenerlytics.eagle.tester.app.models.TestStatus.STATUS.LOGGING;
import static com.mobileenerlytics.eagle.tester.app.models.TestStatus.STATUS.STOPPING;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class TestNotifier {

    @Inject
    TestNotifier(TestStatus testStatus, final Context context) {
        Observable.OnPropertyChangedCallback callback = new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                int status = ((ObservableInt)sender).get();
                if (status == LOGGING) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(context, "Test started", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else if (status == STOPPING) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(context, "Test completed", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        };
        testStatus.status.addOnPropertyChangedCallback(callback);
    }
}
