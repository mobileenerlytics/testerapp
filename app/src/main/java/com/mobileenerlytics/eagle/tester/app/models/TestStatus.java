package com.mobileenerlytics.eagle.tester.app.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.databinding.ObservableLong;
import android.support.annotation.IntDef;
import android.util.Log;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * TestStatus maintains the state of the app. All functionality within the app references the state
 * here to guarantee the app is in a proper state before executing. State is databound to
 * TestingFragment so UI is updated with changes to state.
 *
 * For Testing the app moves from READY-> STARTING -> LOGGING -> STOPPING
 * For Uploading/Deleting TestInfo's the app moves from READY -> PROCESSING
 *
 * When a test begins TestStatus will create a new TestInfo and populate its packageName with
 * package name of AUT, a user entered testName, and commitHash as AUT-<AUT-version>.
 *
 * @author Chase Porter
 * @since 2019-04-08
 */
@Singleton
public class TestStatus extends BaseObservable {
    private final String TAG = TestStatus.class.getSimpleName();
    public final ObservableInt status = new ObservableInt(STATUS.READY);
    public TestInfo lastTestInfo;
    public final ObservableField<String> testName = new ObservableField<>("");
    public final ObservableLong selectedDuration = new ObservableLong(0);
    public final ObservableInt uploadFailed = new ObservableInt(UPLOAD_ERROR.SUCCESS);

    @Inject
    public TestStatus() {
        setState(STATUS.READY);
    }

    /** IntDef enumerating different Error Types. More efficient than enum because stored as int */
    @IntDef({UPLOAD_ERROR.AUTH_FAIL, UPLOAD_ERROR.NETWORK_FAIL, UPLOAD_ERROR.ZIP_FAIL, UPLOAD_ERROR.SUCCESS})
    @Retention(RetentionPolicy.SOURCE)
    public @interface UPLOAD_ERROR {
        int AUTH_FAIL = 401;
        int NETWORK_FAIL = 502;
        int ZIP_FAIL = -2;
        int SUCCESS = 200;
    }

    /** IntDef enumerating different STATUS states. More efficient than enum because stored as int */
    @IntDef({STATUS.READY, STATUS.STARTING, STATUS.LOGGING, STATUS.STOPPING, STATUS.PROCESSING})
    @Retention(RetentionPolicy.SOURCE)
    public @interface STATUS {
        int READY = 0;
        int STARTING = 1;
        int LOGGING = 2;
        int STOPPING = 3;
        int PROCESSING = 4;
    }

    /**
     * Function to set the state of TestStatus. Only functions within TestStatus should call setState
     *
     * @param newStatus - STATUS to set state to
     * @return boolean indicating successful state set
     */
    public synchronized boolean setState(@STATUS int newStatus) {
        this.status.set(newStatus);
        // Need this for now. Ui bound to state functions atm.
        notifyChange();
        return true;
    }

    /** Should only set state to READY from WAITING or STOPPING */
    public synchronized boolean setReady() {
        if (!isStopping() && !isProcessing()) {
            Log.e(TAG, "Attempting to set state to Ready from a logging state.");
            return false;
        }
        return setState(STATUS.READY);
    }

    /** Should only set state to STARTING from READY */
    public synchronized boolean setStarting() {
        if (!isReady()) {
            Log.e(TAG, "Attempting to start test from non-Ready state.");
            return false;
        }
        return setState(STATUS.STARTING);
    }

    /** Should only set state to LOGGING from STARTING */
    public synchronized boolean setLogging() {
        if (!isStarting()) {
            Log.e(TAG, "Attempting to Log from a non-Starting state.");
            return false;
        }
        return setState(STATUS.LOGGING);
    }

    /** Should only set state to STOPPING from LOGGING */
    public synchronized boolean setStopping() {
        if (!isLogging()) {
            Log.e(TAG, "Attempting to stop Logging from a non-Logging state.");
            return false;
        }
        return setState(STATUS.STOPPING);
    }

    /** Should only set state to PROCESSING from READY */
    public synchronized boolean setProcessing() {
        if (!isReady()) {
            Log.e(TAG, "Attempting to Upload while already Uploading or Test in Progress.");
            return false;
        }
        return setState(STATUS.PROCESSING);
    }


    /**
     * Function called to initialize test in the app.
     *
     * @param pkgName - packageName of AUT
     * @param appVersion - AUT-<versionNumber>
     * @param testName - Entered testName: default "manual-test"
     */
    public void initializeTest(String pkgName, String appVersion, String testName) {
        lastTestInfo = new TestInfo(pkgName, appVersion, testName);
    }

    @Bindable
    public synchronized int getState() {
        return this.status.get();
    }

    @Bindable
    public boolean isReady() {
        return this.status.get() == STATUS.READY;
    }

    @Bindable
    public boolean isLogging() {
        return this.status.get() == STATUS.LOGGING;
    }

    @Bindable
    public boolean isProcessing() { return this.status.get() == STATUS.PROCESSING; }

    @Bindable
    public boolean isStarting() {
        return this.status.get() == STATUS.STARTING;
    }

    @Bindable
    public boolean isStopping() {
        return this.status.get() == STATUS.STOPPING;
    }
}
