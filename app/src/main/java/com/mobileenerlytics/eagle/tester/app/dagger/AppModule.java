package com.mobileenerlytics.eagle.tester.app.dagger;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;

import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

/**
 * Module for Dagger2 dependency injection. Provides methods that provide any component that
 * Dagger needs to inject somewhere that is not a class in the current project.
 *
 * @author Chase Porter
 * @since 2019-04-08
 */
@Module
public class AppModule {
    private final PackageManager packageManager;
    private final Context context;
    private final SharedPreferences sharedPreferences;

    public AppModule(PackageManager packageManager, Context context) {
        this.packageManager = packageManager;
        this.context = context;
        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Provides @Singleton
    PackageManager providesPackageManager() {
        return packageManager;
    }

    @Provides @Singleton
    Context providesContext() {
        return context;
    }

    @Provides @Singleton
    SharedPreferences providesSharedPreferences() {
        return sharedPreferences;
    }

    @Provides @Singleton
    OkHttpClient providesOkHttpClient() { return new OkHttpClient(); }
}

