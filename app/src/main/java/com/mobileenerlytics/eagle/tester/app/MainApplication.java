package com.mobileenerlytics.eagle.tester.app;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;

import com.mobileenerlytics.eagle.tester.app.controllers.MessageBroadcastReceiver;
import com.mobileenerlytics.eagle.tester.app.dagger.AppComponent;
import com.mobileenerlytics.eagle.tester.app.dagger.AppModule;
import com.mobileenerlytics.eagle.tester.app.dagger.DaggerAppComponent;
import com.mobileenerlytics.eagle.tester.app.util.MyServiceConnection;

/**
 * MainApplication is in charge of holding the Dagger dependency graph. As long as MainApplication is still alive
 * then everything in the dependency graph will behave as a singleton and will not be garbage collected. All classes
 * injected with classes in the dependency graph will have access to the same instance.
 */
public class MainApplication extends Application {
    AppComponent appComponent;
    MyServiceConnection myServiceConnection;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(getPackageManager(), getApplicationContext())).build();
        appComponent.getTestList().populateTracesList();
        myServiceConnection = appComponent.getMyServiceConnection();
        Intent intent = new Intent(this, MessageService.class);
        getApplicationContext().bindService(intent, myServiceConnection, Context.BIND_AUTO_CREATE);
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        unbindService(myServiceConnection);
    }

}
