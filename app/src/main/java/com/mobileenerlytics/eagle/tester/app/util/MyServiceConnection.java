package com.mobileenerlytics.eagle.tester.app.util;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.os.IBinder;
import android.os.Messenger;
import android.util.Log;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MyServiceConnection implements ServiceConnection {
    public ObservableField<Messenger> mService = new ObservableField<>();
    private final String TAG = MyServiceConnection.class.getSimpleName();

    @Inject
    public MyServiceConnection() {
        super();
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        Log.i(TAG, "Service Connection has started");
        mService.set(new Messenger(service));
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        mService.set(null);
    }

    public Messenger getMessenger() {
        return mService.get();
    }

    public boolean isBound() {
        return mService.get() != null;
    }
}
