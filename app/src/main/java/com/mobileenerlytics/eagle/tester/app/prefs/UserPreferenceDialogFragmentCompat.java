package com.mobileenerlytics.eagle.tester.app.prefs;

import android.os.Bundle;
import android.support.v7.preference.PreferenceDialogFragmentCompat;
import android.view.View;
import android.widget.EditText;

import com.mobileenerlytics.eagle.tester.app.MainApplication;
import com.mobileenerlytics.eagle.tester.app.R;
import com.mobileenerlytics.eagle.tester.app.controllers.AuthTask;
import javax.inject.Inject;

public class UserPreferenceDialogFragmentCompat extends PreferenceDialogFragmentCompat {
    private EditText userView;
    private EditText passView;
    @Inject
    AuthTask authTask;

//    public void setKey(String key) {
    public static UserPreferenceDialogFragmentCompat newInstance(String key) {
        final UserPreferenceDialogFragmentCompat fragment = new UserPreferenceDialogFragmentCompat();
        final Bundle bundle = new Bundle(1);
        bundle.putString(ARG_KEY, key);
//        setArguments(bundle);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        UserPreference preference = (UserPreference) getPreference();
        userView = view.findViewById(R.id.getUsername);
        passView = view.findViewById(R.id.getPassword);

        userView.setText(preference.getUserName());
        passView.setText(preference.getPassword());
    }

    @Override
    public void onDialogClosed(boolean positiveResult) {
        if (!positiveResult)
            return;
        String user = userView.getText().toString();
        String pass = passView.getText().toString();
        UserPreference preference = (UserPreference) getPreference();
        preference.persist(user, pass);
        preference.setSummary(user);
        ((MainApplication)getContext().getApplicationContext()).getAppComponent().inject(this);
        authTask.execute();
    }
}