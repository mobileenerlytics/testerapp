package com.mobileenerlytics.eagle.tester.app.models;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.databinding.Observable;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.VisibleForTesting;

import com.mobileenerlytics.eagle.tester.app.views.SpinnerAdapter;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * AppList holds an AppInfo object for each Application on a device inside an ObservableList used
 * to populate the app dropdown menu for selection
 *
 * @author Chase Porter
 * @since 2019-04-08
 */
@Singleton
public class AppList {
    private PackageManager packageManager;

    public final ObservableArrayList<AppInfo> appInfos = new ObservableArrayList<>();
    public final ObservableInt selected = new ObservableInt(0);
    public final ObservableField<AppInfo> selectedApp = new ObservableField<>();
    public HashSet<String> regPkgs;
    public SpinnerAdapter adapter;

    @Inject
    public AppList(PackageManager packageManager) {
        regPkgs = new HashSet<>();
        adapter = new SpinnerAdapter(appInfos);
        selected.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                selectedApp.set(appInfos.get(selected.get()));
            }
        });
        this.packageManager = packageManager;
        populatePackageList();
    }

    /** Adds an AppInfo to AppList for every Application in PackageManager. */
    @VisibleForTesting
    void populatePackageList() {
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        final List<ResolveInfo> apps = packageManager.queryIntentActivities(mainIntent, 0);
        for (ResolveInfo resolveInfo : apps) {
            String packageName = resolveInfo.activityInfo.packageName;
            if (!regPkgs.contains(packageName)) {
                AppInfo appInfo = new AppInfo(packageName, packageManager);
                appInfos.add(appInfo);
                regPkgs.add(packageName);
            }
        }
        Collections.sort(appInfos);
        selectedApp.set(appInfos.get(0));
    }

    public void setSelectedAppByPackage(String pkgName) {
        for (int i = 0; i < appInfos.size(); i++) {
            if (appInfos.get(i).pkgName.get().equals(pkgName)) {
                selected.set(i);
                break;
            }
        }
    }
}
