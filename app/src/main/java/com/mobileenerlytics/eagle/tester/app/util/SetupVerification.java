package com.mobileenerlytics.eagle.tester.app.util;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.util.Log;

import com.mobileenerlytics.eagle.tester.app.bridge.AppDevice;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class SetupVerification {
    private static final String TAG = "SetupVerification";
    private static AppDevice appDevice;

    public static boolean verify(Context ctx) {
        if(appDevice == null)
            appDevice = new AppDevice(ctx);
        return verifySettings(ctx) && verifyStorage(ctx) && verifyRooted();
    }

    public static boolean verifySettings(Context context) {
        return Settings.System.canWrite(context);
    }

    public static boolean verifyStorage(Context ctx) {
        return (ctx.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    public static boolean verifyRooted() {
        try {
            Future<String> future = appDevice.executeShellCommand("echo test", 5, TimeUnit.SECONDS);
            String out = future.get();
            if (out.contains("test"))
                return true;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        return false;
    }
}
