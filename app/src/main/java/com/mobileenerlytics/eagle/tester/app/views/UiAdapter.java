package com.mobileenerlytics.eagle.tester.app.views;

import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ProgressBar;

import com.mobileenerlytics.eagle.tester.app.models.TestStatus;

/** All functions UI components need to call */
public class UiAdapter {
    @BindingAdapter({"enabled"})
    public static void setEnabled(View view, boolean enable) {
        if (enable) {
            view.setAlpha(1);
        } else {
            view.setAlpha((float) 0.5);
        }
        view.setEnabled(enable);
    }

    @BindingAdapter({"visibility"})
    public static void setVisibility(View view, boolean visible) {
        int visibility =  visible ? View.VISIBLE : View.GONE;
        view.setVisibility(visibility);
    }

    @BindingAdapter({"chronbase"})
    public static void setBase(Chronometer chronometer, TestStatus status) {
        if(status.isLogging()) {
            chronometer.start();
            chronometer.setBase(status.lastTestInfo.startRealTime.get());
        } else {
            chronometer.stop();
        }
    }

    @BindingAdapter({"progress"})
    public static void setProgress(ProgressBar progressBar, double progress) {
        progressBar.setProgress((int)progress);
    }
}
