package com.mobileenerlytics.eagle.tester.app.loggers;

import android.os.BatteryManager;
import android.util.Log;

import com.google.common.collect.Sets;
import com.mobileenerlytics.eprof.logger.EprofLogger;
import com.mobileenerlytics.eprof.logger.EprofLoggerMain;

import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import static android.os.BatteryManager.BATTERY_PROPERTY_CURRENT_NOW;

public class CurrentSensorLogger extends EprofLogger {
    final String TAG = CurrentSensorLogger.class.getSimpleName();
    LinkedHashMap<Long, Long> currentLogs;
    BatteryManager batteryManager;
    Thread monitorThread;

    public CurrentSensorLogger(EprofLoggerMain loggerMain, BatteryManager batteryManager) {
        super(loggerMain);
        this.batteryManager = batteryManager;
        forComponent = Sets.newHashSet(EprofLoggerMain.LoggedComponent.CURRENT_SENSOR);
    }

    @Override
    public boolean start() {
        currentLogs = new LinkedHashMap<>();
        monitorThread = new Thread("current-monitor thread") {
            int SLEEP_TIME = 200;
            @Override
            public void run() {
                while (!isInterrupted()) {
                    try {
                        long startTime = mLoggerMain.getStartTimeMillis();
                        if (startTime != 0) {
                            long time = System.currentTimeMillis() - startTime;
                            long current = batteryManager.getLongProperty(BATTERY_PROPERTY_CURRENT_NOW);
                            currentLogs.put(time, current);
                        }
                        Thread.sleep(SLEEP_TIME);
                    } catch (InterruptedException e) {
                        return;
                    }
                }
            }
        };
        Log.i(TAG, "Starting current monitoring");
        monitorThread.start();
        return true;
    }

    @Override
    public void reset() {
        _stop();
    }

    @Override
    public void stop() {
        _stop();
        try (FileWriter fw = new FileWriter(mLoggerMain.getLogDir() + "/current_sensor", false)) {
            for (Map.Entry<Long, Long> currentEntry : currentLogs.entrySet()) {
                fw.write(currentEntry.getKey() + ", " + currentEntry.getValue() + "\n");
            }
        } catch (IOException e) {
            Log.e(TAG, e.toString());
        }
        mLoggerMain.finish(this);
    }

    public void _stop() {
        monitorThread.interrupt();
        try {
            monitorThread.join();
        } catch (InterruptedException e1) {
            Log.e(TAG, e1.toString());
        }
        Log.i(TAG,"Stopped current monitor thread");
    }
}
