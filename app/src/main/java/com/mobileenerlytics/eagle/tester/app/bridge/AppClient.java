package com.mobileenerlytics.eagle.tester.app.bridge;

import android.app.ActivityManager;

import com.mobileenerlytics.eprof.logger.bridge.ClientBridge;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by abhilash on 13/10/17.
 */

public class AppClient extends ClientBridge {
    final ActivityManager.RunningAppProcessInfo mAppProcessInfo;
    private static final Map<String, AppClient> lookupTable = new HashMap<>();
    @Override
    public String getPkgName() {
        return mAppProcessInfo.processName;
    }

    @Override
    public void kill() {
        throw new UnsupportedOperationException("Killing clients aren't supported");
    }

    private AppClient(ActivityManager.RunningAppProcessInfo appProcessInfo) {
        mAppProcessInfo = appProcessInfo;
    }


    public static ClientBridge getClient(ActivityManager.RunningAppProcessInfo appProcessInfo) {
        if(!lookupTable.containsKey(appProcessInfo.processName))
            lookupTable.put(appProcessInfo.processName, new AppClient(appProcessInfo));
        return lookupTable.get(appProcessInfo.processName);
    }
}