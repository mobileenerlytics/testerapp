package com.mobileenerlytics.eagle.tester.app.views;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.ObservableArrayList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Spinner;

import com.mobileenerlytics.eagle.tester.app.databinding.ListviewAppBinding;
import com.mobileenerlytics.eagle.tester.app.models.AppInfo;
import com.mobileenerlytics.eagle.tester.app.models.AppList;

public class SpinnerAdapter extends BaseAdapter {
    LayoutInflater layoutInflater;
    ObservableArrayList<AppInfo> appList;
    private static SpinnerAdapter mInstance;

    public SpinnerAdapter(ObservableArrayList<AppInfo> mAppList) {
        appList = mAppList;
    }

    public static SpinnerAdapter getInstance(ObservableArrayList<AppInfo> appList) {
        if (mInstance == null)
            mInstance = new SpinnerAdapter(appList);
        return mInstance;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        ListviewAppBinding binding;
        if (view == null) {
            if (layoutInflater== null) {
                layoutInflater = (LayoutInflater) parent.getContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            binding = ListviewAppBinding.inflate(
                    layoutInflater, parent, false);
            view = binding.getRoot();
            view.setTag(binding);
        }
        else {
            binding = (ListviewAppBinding) view.getTag();
        }
        binding.setData(appList.get(i));
        return view;
    }

    @Override
    public int getCount() {
        return appList.size();
    }

    @Override
    public Object getItem(int i) {
        return appList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @BindingAdapter({"entries"})
    public static void setEntries(Spinner spinner, AppList appList) {
        spinner.setAdapter(getInstance(appList.appInfos));
        spinner.setSelection(appList.selected.get());
    }
}
