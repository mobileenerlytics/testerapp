package com.mobileenerlytics.eagle.tester.app.models;

import android.databinding.BaseObservable;
import android.databinding.Observable;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableInt;
import android.os.Environment;
import android.support.annotation.VisibleForTesting; import android.util.Log;

import com.mobileenerlytics.eagle.tester.app.controllers.UploadWorker;
import com.mobileenerlytics.eagle.tester.app.tasks.DeleteTask;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Properties;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import static com.mobileenerlytics.eagle.tester.app.prefs.PrefUtils.PACKAGE_KEY;
import static com.mobileenerlytics.eagle.tester.app.prefs.PrefUtils.COMMIT_KEY;
import static com.mobileenerlytics.eagle.tester.app.prefs.PrefUtils.DURATION_MS_KEY;
import static com.mobileenerlytics.eagle.tester.app.prefs.PrefUtils.TEST_KEY;
import static com.mobileenerlytics.eagle.tester.app.models.TestInfo.STATUS.UNCHECKED;
import static com.mobileenerlytics.eagle.tester.app.models.TestInfo.STATUS.CHECKED;
import static com.mobileenerlytics.eagle.tester.app.models.TestInfo.STATUS.STAGED;

/**
 * Model for holding TestInfo's of executed tests. Manages creating TestInfo's for tests already in
 * trace directory on app launch, getting checked or processing tests, staging tests to be uploaded,
 * scheduling tests to be uploaded, and deleting tests.
 *
 * TestList is a Singleton and the same instance is accessed by all classes that use it.
 *
 * @author Chase Porter
 * @since 2019-04-08
 */
@Singleton
public class TestList extends BaseObservable {
    public String TAG = TestList.class.getSimpleName();
    public ObservableArrayList< TestInfo> testInfos;
    public ObservableArrayList<TestInfo> checkedTestInfos;
    public ObservableArrayList<TestInfo> processingTestInfos;

    @Inject
    public TestList() {
        testInfos = new ObservableArrayList<>();
        checkedTestInfos = new ObservableArrayList<>();
        processingTestInfos = new ObservableArrayList<>();
    }

    /** @return array of checked TestInfo's */
    public TestInfo[] getChecked() {
        return checkedTestInfos.toArray(new TestInfo[checkedTestInfos.size()]);
    }

    /** @return array of processing TestInfo's */
    public TestInfo[] getProcessing() {
        return processingTestInfos.toArray(new TestInfo[processingTestInfos.size()]);
    }

    /**
     * Function to add a TestInfo to the TestList. Adds testInfo to testInfos if not already there and
     * adds callback to state of testInfo to update checked and processing lists correctly.
     *
     * @param testInfo - testInfo to add to TestList
     */
    public void add(final TestInfo testInfo) {
        if (testInfos.contains(testInfo))
            return;
        testInfos.add(0, testInfo);
        Observable.OnPropertyChangedCallback callback = new OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                synchronized (testInfo) {
                    int status = ((ObservableInt) sender).get();
                    if (status == STAGED && !processingTestInfos.contains(testInfo)) {
                        checkedTestInfos.remove(testInfo);
                        processingTestInfos.add(testInfo);
                    } else if (status == CHECKED) {
                        checkedTestInfos.add(testInfo);
                        processingTestInfos.remove(testInfo);
                    } else if (status == UNCHECKED) {
                        checkedTestInfos.remove(testInfo);
                        processingTestInfos.remove(testInfo);
                    }
                }
            }
        };
        testInfo.state.addOnPropertyChangedCallback(callback);
    }

    /** Stages all checked tests: staged to upload, but has not yet begun uploading */
    private void stageTests() {
        for (TestInfo testInfo : getChecked()) {
            testInfo.stageTest();
        }
    }

    /** Stages all tests currently processing, happens if UploadJob is interrupted by a bad network
     *  connection for example. */
    public void reStageTests() {
        for (TestInfo testInfo : getProcessing()) {
            testInfo.stageTest();
        }
    }

    /** Stages checked tests and schedules an UploadWorker with WorkManager. Will run when connected to
     *  WiFi */
    public void scheduleUpload() {
        stageTests();
        Constraints uploadConstraints = new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build();
        OneTimeWorkRequest uploadWork = new OneTimeWorkRequest.Builder(UploadWorker.class).setConstraints(uploadConstraints).build();
        WorkManager.getInstance().enqueue(uploadWork);
    }

    /** Removes a TestInfo from all arrays managed in TestList */
    public void remove(TestInfo testInfo) {
        testInfos.remove(testInfo);
        checkedTestInfos.remove(testInfo);
        processingTestInfos.remove(testInfo);
    }

    /** Removes all TestInfo's in a collection from TestList */
    public void removeAll(Collection<TestInfo> toRemove) {
        for (TestInfo testInfo : toRemove) {
            // WARNING: don't use .removeAll(Collection) on an ObservableArrayList, method is not implemented.
            remove(testInfo);
        }
    }

    /** Executes a DeleteTask on all checked TestInfo's */
    public void delete(TestStatus status) {
        new DeleteTask(status, this).execute(getChecked());
    }

    /** Cancels WorkManager and calls cancelUpload on all TestInfo's currently processing */
    public void cancelAllUploads() {
        // Don't change TestStatus to READY here. First unblock UploadWorker from awaitTermination and then set TestStatus to READY
        WorkManager.getInstance().cancelAllWork();
        for (TestInfo testInfo : getProcessing())
            testInfo.cancelUpload();
    }

    /** Checks all TestInfo's in TestList */
    public void checkAll(boolean checked) {
        for (TestInfo testInfo : testInfos) {
            if (!checked || testInfo.isUnchecked())
                testInfo.checkTest();
        }
    }

    /** Creates a testInfo from a previously existing directory in the traces folder */
    @VisibleForTesting
    TestInfo createTestInfo(File traceFile) {
        String fName = traceFile.getAbsolutePath();
        File[] logFiles = traceFile.listFiles();
        String fPackage = "<not a trace log>";
        String testName = "";
        String commitHash = "";
        String durationInMs = null;
        if (logFiles != null && logFiles.length != 0) {
            for (File logFile : logFiles) {
                if (logFile.getName().equals("build.prop")) {
                    try {
                        BufferedReader bufferedReader = new BufferedReader(new FileReader(logFile));
                        Properties properties = new Properties();
                        properties.load(bufferedReader);
                        fPackage = properties.getProperty(PACKAGE_KEY);
                        testName = properties.getProperty(TEST_KEY);
                        commitHash = properties.getProperty(COMMIT_KEY);
                        durationInMs = properties.getProperty(DURATION_MS_KEY);
                    } catch (Exception e) {
                        Log.e("TRACE", e.toString());
                    }
                }
            }
        }
        TestInfo prevTest = new TestInfo(fPackage, commitHash, testName);
        prevTest.startRealTime.set(System.currentTimeMillis());
        prevTest.logDir.set(fName);
        try {
            prevTest.setDuration(Long.parseLong(durationInMs));
        } catch(NumberFormatException nfe) {
            Log.e(TAG, "No test duration found in build.prop for test located at " + fName);
        }
        return prevTest;
    }

    /** Create a TestInfo and add it to TestList for every Test already in traces */
    public void populateTracesList() {
        if (testInfos != null)
            testInfos.clear();
        File externalStorage = Environment.getExternalStorageDirectory();
        File traceDir = new File(externalStorage.getAbsolutePath() + "/traces");
        File[] traceFiles = traceDir.listFiles();
        if(traceFiles != null) {
            Arrays.sort(traceFiles, new Comparator<File>() {
                public int compare(File f1, File f2) {
                    return Long.valueOf(f1.lastModified()).compareTo(Long.valueOf(f2.lastModified()));
                }
            });
            for (File traceFile : traceFiles)
                add(createTestInfo(traceFile));
        }
    }
}
