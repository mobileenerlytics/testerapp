#ifndef TESTERAPP_TRACEPRODUCER_H
#define TESTERAPP_TRACEPRODUCER_H

#include "bufferpool.h"

void producer_start(BufferPool *pool, char *pip_path);
void producer_stop(int signum);

// For testing
extern void producer_fill_buffer(FILE *file, BufferNode *node);

#endif //TESTERAPP_TRACEPRODUCER_H
