#ifndef TESTERAPP_TRACEMAIN_H
#define TESTERAPP_TRACEMAIN_H

void* start_consumer_thread(void *threadargs);
void* start_producer_thread(void *threadargs);

#endif //TESTERAPP_TRACEMAIN_H
