#include <stdio.h>
#include <pthread.h>
#include <android/log.h>
#include <cerrno>
#include <cstring>
#include "traceproducer.h"

#define CHUNK 8*1024
#define APPNAME "EagleTester"
int producer_interrupted = 0;

void producer_read_pipe_to_buffers(BufferPool *pool, FILE *file);
void producer_fill_buffer(FILE *file, BufferNode *node);

void producer_start(BufferPool *pool, char *pipe_path) {
    FILE *trace_pipe = fopen(pipe_path, "rb");
    if(trace_pipe == NULL) {
        __android_log_print(ANDROID_LOG_ERROR, APPNAME, "Failed to open trace_pipe %s\n", strerror(errno));
        return;
    }
    producer_read_pipe_to_buffers(pool, trace_pipe);
    fclose(trace_pipe);
}

void producer_read_pipe_to_buffers(BufferPool *pool, FILE *file) {
    BufferNode *node;
    __android_log_print(ANDROID_LOG_VERBOSE, APPNAME, "=== traceproducer starting ===\n");
    producer_interrupted = 0;
    while (!producer_interrupted) {
        __android_log_print(ANDROID_LOG_VERBOSE, APPNAME, "=== Getting free buffer ===\n");
        node = pool->get_free_buffer();
        __android_log_print(ANDROID_LOG_VERBOSE, APPNAME, "=== Got free buffer: %p ===\n", node);
        producer_fill_buffer(file, node);
        pool->mark_full();
    }
    __android_log_print(ANDROID_LOG_VERBOSE, APPNAME, "=== traceproducer exiting ===\n");
    pthread_exit(0);
}

void producer_fill_buffer(FILE *file, BufferNode *node) {
    // Unlike read, fread reads the whole buffer in one go. Any less is an error.
    // Calling fread in smaller chunks, since there's no clean way to interrupt fread when we want to
    // stop a test
    size_t remaining_capacity = node->remaining_capacity();
    while(remaining_capacity > 0 && !producer_interrupted) {
        size_t pre_size = node->size;
        size_t pre_cap = remaining_capacity;

        size_t toread = CHUNK < remaining_capacity ? CHUNK : remaining_capacity;

        size_t num_read = fread(node->buffer + node->size, sizeof(char), toread, file);
        node->size += num_read;
        remaining_capacity = node->remaining_capacity();
    }
}

void producer_stop(int signum) {
    printf("=== tracereducer interrupt ===\n");
    __android_log_print(ANDROID_LOG_VERBOSE, APPNAME, "=== tracereducer interrupted ===\n");
    producer_interrupted = 1;
//    consumer_stop();
}