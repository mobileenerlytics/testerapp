#ifndef TESTERAPP_BUFFERNODE_H
#define TESTERAPP_BUFFERNODE_H

#include <assert.h>
#include <cstdio>

extern void throw_assert(bool EXPRESSION);

class BufferNode {
public:
    size_t size;
    size_t capacity;
    size_t index;
    BufferNode* next_node;
    char *buffer;
    size_t remaining_capacity();
    BufferNode(size_t buf_size);
    ~BufferNode();
};

#endif