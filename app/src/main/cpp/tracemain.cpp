#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "traceproducer.h"
#include "traceconsumer.h"
#include "tracemain.h"
#include "bufferpool.h"
#include <signal.h>
#include <android/log.h>
#include <jni.h>

#define APPNAME "EagleTester"
#define NUM_BUFFERS 5
#define BUFFER_SIZE 32 * 1024 * 1024
#define NUM_CONSUMERS 3

pthread_t producer_thread;
pthread_t consumer_thread[NUM_CONSUMERS];

static BufferPool* pool;
static char *inputFile;
static char *outputDir;

int
main(int argc, char *argv[])
{
    assert(argc ==3);
    signal(SIGINT, producer_stop);
    signal(SIGTERM, producer_stop);

    printf("=== tracereducer starting ===\n");
    __android_log_print(ANDROID_LOG_VERBOSE, APPNAME, "=== tracereducer starting ===\n");
    pool = new BufferPool(NUM_BUFFERS, BUFFER_SIZE);

    // Start producer thread
    inputFile = argv[1];
    outputDir = argv[2];
    pthread_create(&producer_thread, NULL, start_producer_thread, NULL);
    pthread_setname_np(producer_thread, "trace_producer_thread");

    // Start consumer threads
    for (size_t i = 0; i < NUM_CONSUMERS; i++) {
        pthread_create(&(consumer_thread[i]), NULL, start_consumer_thread, NULL);
    }

    pthread_setname_np(consumer_thread[0], "trace_consumer_thread_A");
    pthread_setname_np(consumer_thread[1], "trace_consumer_thread_B");

    pthread_join(producer_thread, NULL);
    printf("=== tracereducer stopping ===\n");

    delete pool;
    printf("=== tracereducer stopped ===\n");
    __android_log_print(ANDROID_LOG_VERBOSE, APPNAME, "=== tracereducer stopped ===\n");
    return 0;
}

void *
start_producer_thread(void *args) {
    producer_start(pool, inputFile);
    return NULL;
}

void *
start_consumer_thread(void *args) {
    consumer_start(pool, outputDir);
    return NULL;
}

