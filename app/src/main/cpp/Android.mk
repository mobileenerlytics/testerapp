LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE     := tracereduce
FILE_LIST = $(wildcard $(LOCAL_PATH)/*.cpp)
LOCAL_SRC_FILES = $(FILE_LIST:$(LOCAL_PATH)/%=%)
LOCAL_LDLIBS += -lz -llog
#LOCAL_CFLAGS += -fprofile-arcs -ftest-coverage
#LOCAL_LDFLAGS += --coverage

#include $(BUILD_SHARED_LIBRARY)
include $(BUILD_EXECUTABLE)

#include $(CLEAR_VARS)
#
#LOCAL_MODULE := tracereducetest
#FILE_LIST := $(wildcard $(LOCAL_PATH)/../../androidTest/cpp/*.cpp)
#LOCAL_SHARED_LIBRARIES := tracereduce
#LOCAL_SRC_FILES += $(FILE_LIST:$(LOCAL_PATH)/%=%)
#LOCAL_STATIC_LIBRARIES := googletest_main
#LOCAL_LDLIBS += -lz -llog
#
#include $(BUILD_EXECUTABLE)
##include $(BUILD_SHARED_LIBRARY)
#
#$(call import-module,third_party/googletest)
