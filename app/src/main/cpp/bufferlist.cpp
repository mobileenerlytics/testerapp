#include <stddef.h>
#include <malloc.h>
#include <pthread.h>
#include "bufferlist.h"

BufferList::BufferList() : len(0), head(NULL) {
    pthread_mutex_init(&mutex, NULL);
    sem_init(&semaphore, 0, 0);
    pthread_cond_init(&list_condition, NULL);
}

void BufferList::push(BufferNode *new_node) {
    throw_assert(new_node != NULL);
    pthread_mutex_lock(&mutex);
    if (head == NULL) {
        head = new_node;
    } else {
        BufferNode *temp_node = head;
        while (temp_node->next_node != NULL) {
            throw_assert(temp_node != new_node);
            temp_node = temp_node->next_node;
        }
        throw_assert(temp_node != new_node);
        temp_node->next_node= new_node;
    }
    len++;
    pthread_mutex_unlock(&mutex);
    sem_post(&semaphore);
    pthread_cond_signal(&list_condition);
}

BufferNode* BufferList::pop() {
    sem_wait(&semaphore);
    pthread_mutex_lock(&mutex);
    throw_assert(head != NULL);
    BufferNode *temp_node = head;
    head = head->next_node;
    temp_node->next_node = NULL;
    len--;
    pthread_mutex_unlock(&mutex);
    pthread_cond_signal(&list_condition);
    return temp_node;
}

BufferList::~BufferList() {
    while(len > 0) {
        delete pop();
    }
    sem_destroy(&semaphore);
    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&list_condition);
}

void BufferList::wait_till_num(int num_buffers) {
    pthread_mutex_lock(&mutex);
    while (len != num_buffers) {
        pthread_cond_wait(&list_condition, &mutex);
    }
    pthread_mutex_unlock(&mutex);
}
