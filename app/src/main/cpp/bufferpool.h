#ifndef TESTERAPP_BUFFERPOOL_H
#define TESTERAPP_BUFFERPOOL_H

#include <stdint.h>
#include <semaphore.h>
#include "bufferlist.h"
#include "buffernode.h"

class BufferPool {
    size_t num_buffers;
    size_t buffer_size;
    size_t current_index;
    BufferNode* producer_buffer;
    BufferList* free_buffers_list;
    BufferList* full_buffers_list;

public:
    BufferPool(size_t num_buffers, size_t buf_size);

    BufferNode* get_free_buffer();

    void mark_full();

    BufferNode* get_full_buffer();

    void mark_free(BufferNode* node);

    size_t get_buffer_size() {
        return buffer_size;
    }

    ~BufferPool();
};


#endif //TESTERAPP_BUFFERPOOL_H
