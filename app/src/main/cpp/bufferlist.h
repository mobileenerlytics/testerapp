#ifndef TESTERAPP_LINKEDLIST_H
#define TESTERAPP_LINKEDLIST_H

#include <semaphore.h>
#include <pthread.h>
#include "buffernode.h"

class BufferList {
private:
    BufferNode* head;
    sem_t semaphore;
    pthread_mutex_t mutex;
    size_t len;
    pthread_cond_t list_condition;
public:
    BufferList();

    void push(BufferNode* new_node);

    BufferNode* pop();

    void wait_till_num(int num_buffers);

    ~BufferList();

    size_t length() {
        return len;
    }
};


#endif //TESTERAPP_LINKEDLIST_H
