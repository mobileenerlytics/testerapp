#include <zlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <android/log.h>
#include <cerrno>
#include "stdio.h"
#include "bufferpool.h"
#include "traceconsumer.h"

#define APPNAME "EagleTester"
#define MAX_SZ 2048

int mget_line(char *buffer, int buff_size, int start);
int is_ctx_switch(char line_string[], char *line, size_t size);
size_t reduce_line(char *line);
ssize_t writeallgzip(gzFile gzipFile, char buffer[], int len);
//ssize_t writeall(int fd, void *buf, size_t len);
void consume_buffer(char line_string[], char *buffer, size_t buf_size, char *outfile, int buf_index);
int is_tracing_mark(char line_string[], char *line, size_t size);

void consumer_start(BufferPool *pool, char *outpath) {
    __android_log_print(ANDROID_LOG_VERBOSE, APPNAME, "=== traceconsumer starting ===\n");
    char line_string[MAX_SZ];
    while (true) {
        char zippath[strlen(outpath) + 8];
        __android_log_print(ANDROID_LOG_VERBOSE, APPNAME, "=== Getting full buffer ===\n");
        BufferNode *node = pool->get_full_buffer(); // blocking
        __android_log_print(ANDROID_LOG_VERBOSE, APPNAME, "=== Got full buffer: %p ===\n", node);
        sprintf(zippath, "%s_%d.gz", outpath, (int)node->index);
        char *buffer = node->buffer;
        consume_buffer(line_string, buffer, node->size, zippath, node->index);
        pool->mark_free(node);
    }
}

void consume_buffer(char line_string[], char *buffer, size_t buf_size, char *outfile, int buf_index) {
    // Skip first line because the line is broken (unless is tracing_mark_write)
    int skip = mget_line(buffer, buf_size, 0);
    if (!is_tracing_mark(line_string, buffer, skip + 1)) {
        buffer =  buffer + ((skip + 1) * sizeof(char));
        buf_size = buf_size - skip - 1;
    }
    gzFile gzipFile = gzopen(outfile, "wb");
    if(gzipFile == NULL) {
        __android_log_print(ANDROID_LOG_ERROR, APPNAME, "Failed to open file %s %s\n", outfile,
                            strerror(errno));
        return;
    }
    long index = 0;
    long line_start = 0;
    int line_size = 0;
    char *line = NULL;
    while (true && ((index = mget_line(buffer, buf_size, line_start)) != -1)) {
        line = buffer + line_start;
        line_size = index - line_start + 1;
        if (is_ctx_switch(line_string, line, line_size)) {
            line_size = reduce_line(line);
        }
        writeallgzip(gzipFile, line, line_size);
        line_start = index + 1;
    }

    gzclose(gzipFile);
}

int mget_line(char *buffer, int buff_size, int start) {
    for (int index = start; index < buff_size; index++) {
        if (buffer[index] == '\n') {
            return index;
        }
    }
    return -1;
}

int is_tracing_mark(char line_string[], char *line, size_t size) {
    assert(size < MAX_SZ);
    memset(line_string, 0, MAX_SZ);
    memcpy(line_string, line, size);
    return strstr(line_string, (char *) "tracing_mark_write") != NULL;
}

int is_ctx_switch(char line_string[], char *line, size_t size) {
    assert(size < MAX_SZ);
    memset(line_string, 0, MAX_SZ);
    memcpy(line_string, line, size);
    return strstr(line_string, (char *) "sched_switch") != NULL;
}

size_t reduce_line(char *line) {
    char *ctx_switch = strstr(line, "sched_switch");
    char *prev_pid = strstr(line, "prev_pid");
    char *prev_prio = strstr(line, "prev_prio");
    int prev_pid_token_size = prev_prio - prev_pid;
    memmove(ctx_switch + 14, prev_pid, prev_pid_token_size);

    char *next_pid = strstr(line, "next_pid");
    char *next_prio = strstr(line, " next_prio");
    next_prio[0] = '\n';
//    next_prio[1] = '\0'; // debug only
    int next_pid_token_size = next_prio - next_pid + 1;

    memmove(ctx_switch + 14 + prev_pid_token_size, next_pid, next_pid_token_size);
    return 14 + (ctx_switch + prev_pid_token_size + next_pid_token_size - line);
}

ssize_t writeallgzip(gzFile gzipFile, char buffer[], int len) {
    ssize_t count = 0;
    while (count < len) {
        int numwritten = gzwrite(gzipFile, buffer + count, len - count);
        if (numwritten < 1) {
            __android_log_print(ANDROID_LOG_ERROR, APPNAME, "=== Failed to write to gzip file. %s ===\n", strerror(errno));
            return numwritten;
        }
        count += numwritten;
    }
    return count;
}

//ssize_t writeall(int fd, void *buf, size_t len) {
//    size_t count = 0;
//    while (count < len) {
//        int i = write(fd, count+(char *)buf, len-count);
//        if (i<1) return i;
//        count += i;
//    }
//    return count;
//}
