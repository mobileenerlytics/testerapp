# Debugging crashes

## Getting backtrace

When `tracereduce` crashes at runtime, a stack trace of crashed thread
is available in `logcat`. Full stack trace, program counters etc for all
threads are available at `/data/tombstones/` which is only accessible as
root user.

## Finding line numbers in backtrace

Pull the crash log (from `logcat` or `tombstone_**`) in a file on your
computer. Navigate to `$ANDROID_NDK_PATH` and locate executable
`ndk-stack`.

Then you shall be able to run the following command to get line numbers:

```
./ndk-stack -sym <path to TesterApp>/app/build/intermediates/ndkBuild/debug/obj/local/armeabi-v7a/ -dump <crash log>  | less
```

## I couldn't get line numbers

Common mistakes:

* The architecture passed to `ndk-stack` command above (like
  `armeabi-v7a`) doesn't match with the phone architecture. Try using
  alternative architecture in the `ndk-stack` command above. 
  
* The `tracereducer` source code has changed from the `tracereducer`
  packaged in `energylogger`. 
  
  * Rebuild `TesterApp` using `gradle build`. This will generate a new
    `tracereducer` executable.
  * Copy over `<Path to
    TesterApp>/app/build/intermediates/ndkBuild/debug/obj/local/armeabi-v7a/tracereduce`
    to `<Path to energylogger>/src/main/resources/`.
  * Rebuild `energylogger` using gradle. This will package new
    `tracereducer` into `energylogger**.jar`
  * Rebuild `Testerapp` with this local `energylogger**jar` as
    dependency (refer `TesterApp/app/build.gradle`).
  * Install new `TesterApp` and repeat the failure condition and collect
    crash logs. You should now be able to use `ndk-stack` successfully.