#include <cstdio>
#include <cstdlib>
#include "buffernode.h"


void throw_assert(bool EXPRESSION) {
    if(!EXPRESSION)
        throw "Assertion failed";
}

size_t BufferNode::remaining_capacity() {
    return capacity - 1 - size;
}

BufferNode::BufferNode(size_t capacity) :
        capacity(capacity), next_node(NULL), size(0) {
    buffer = (char *) calloc(1, capacity * sizeof(char));
    throw_assert(buffer != NULL);
}

BufferNode::~BufferNode(){
    free(buffer);
}
