#ifndef TESTERAPP_TRACECONSUMER_H
#define TESTERAPP_TRACECONSUMER_H

#include <zlib.h>
#include "bufferpool.h"

extern void consumer_start(BufferPool *pool, char *outfile);

// For testing
extern int is_ctx_switch(char line_string[], char *line, size_t size);
extern int is_tracing_mark(char line_string[], char *line, size_t size);
extern size_t reduce_line(char *line);
extern int mget_line(char *buffer, int buff_size, int start);

#endif //TESTERAPP_TRACECONSUMER_H
