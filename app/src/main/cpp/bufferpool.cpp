#include <malloc.h>
#include <string.h>
#include <pthread.h>
#include "bufferpool.h"

BufferPool::BufferPool(size_t num_buffers, size_t buf_size) :
        buffer_size(buf_size),
        num_buffers(num_buffers),
        current_index(0) {
    producer_buffer = NULL;
    free_buffers_list = new BufferList();
    full_buffers_list = new BufferList();
    for(size_t i = 0; i < num_buffers; i++) {
        BufferNode *node = new BufferNode(buf_size);
        free_buffers_list->push(node);
    }
}

BufferNode* BufferPool::get_free_buffer() {
    throw_assert(producer_buffer == NULL);
    producer_buffer = free_buffers_list->pop();
    producer_buffer->index = current_index++;
    return producer_buffer;
}

void BufferPool::mark_free(BufferNode *node) {
    memset(node->buffer, 0, node->capacity);
    node->size = 0;
    free_buffers_list->push(node);
}

BufferNode* BufferPool::get_full_buffer() {
    return full_buffers_list->pop();
}

void BufferPool::mark_full() {
    throw_assert(producer_buffer != NULL);
    full_buffers_list->push(producer_buffer);
    producer_buffer = NULL;
}

BufferPool::~BufferPool() {
    free_buffers_list->wait_till_num(num_buffers);
    throw_assert(producer_buffer == NULL);
    delete free_buffers_list;
    delete full_buffers_list;
}
